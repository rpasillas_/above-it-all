<?php

/* Template Name: Treatment Process */

get_header(); ?>
<?php

	$child_pages = wp_get_nav_menu_items(105);
	$current_page = get_the_ID();

	// echo '<pre>';
	// var_dump($child_pages);
	// echo '</pre>'; 
?>

	<div class="wrap">

		<nav class="tabs-navigation">
			<ul>
	<?php 
		//DISPLAY ALL CHILD PAGES
		if ( $child_pages ) : 
			foreach ( $child_pages as $pageChild ) : 
				$id = get_post_meta( $pageChild->ID, '_menu_item_object_id', true );

				$child_current = false;

				if($id == $current_page){
					$child_current = 'class="current_page_item"';
				}

				// echo '<pre>';
				// print_r($id);
				// echo '</pre>';
	?>



	<li <?php echo $child_current; ?> data-item="<?php echo $id; ?>">
		 <?php echo get_the_post_thumbnail($id, 'thumbnail'); ?>
		 <a href="<?php echo get_permalink($id); ?>" rel="bookmark" title="<?php echo $pageChild->post_title; ?>"><?php echo $pageChild->title; ?></a>
	</li>


	<?php endforeach; endif; ?>

			</ul>
		</nav>


		<div class="primary content-area">

			<main id="main" class="site-main" role="main">

				<?php
				while ( have_posts() ) : the_post();

					get_template_part( 'template-parts/content', 'page' );

				endwhile; // End of the loop.
				?>

			</main><!-- #main -->
		</div><!-- .primary -->

		<?php get_sidebar(); ?>

	</div><!-- .wrap -->

<?php get_footer(); ?>