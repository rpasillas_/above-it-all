<?php

/* Template Name: Treatment Funnel */

get_header(); ?>
	<?php
		//----  GET THE PAGE'S ANCESTORS
		global $post;
		$anc = get_post_ancestors( $post->ID );

	    //----  IF $ANC HAS MORE THAN 1 VALUE
		//----  THEN WE HAVE A 3RD LEVEL CHILD PAGE
		//----  AND THUS WANT THE PARENT ID
    	$top_id = $post->ID;
    	if ( count( $anc ) > 1 ){
    		$top_id = $anc[0];
    	}else if( count( $anc ) == 1 ){
    		$top_id = $anc[0];
    	}

    	//---- PREVENT TOP MOST LEVEL PAGE FROM DISPLAYING
    	if( $top_id == 4505 ){
    		$top_id = $post->ID;
    	}

	    //----  TAKEN FROM WP CODEX
		$ancestor_id = $top_id;
		//VERY IMPORTANT, THE ORDERING OF THIS ARRAY
		//ENSURE THE SORTING MATCHES IN WP_LIST_PAGES BELOW
		$descendants = get_pages(array(
					'child_of' => $ancestor_id,
					'sort_order' =>'ASC', 
					'sort_column'  => 'menu_order',
					));
		$incl = "";

		foreach ($descendants as $page) {
			if (($page->post_parent == $ancestor_id) ||
			($page->post_parent == $post->post_parent) ||
			($page->post_parent == $post->ID))
			{
				$incl .= $page->ID . ",";
			}
		}

		//----  MAKE A FINAL ARRAY FOR USE 
		//----  AS THE PREV//NEXT NAVIGATION
		$pages_array = explode( ',', $incl );
		array_pop($pages_array); //pop off last empty value
		array_unshift( $pages_array, $ancestor_id); //add the top level page
		

		//---- CREATE A COPY OF THE FINAL ARRAY
		//---- WE WILL USE THIS TO DETERMINE 
		//---- IF THE CURRENT PAGE IS THE LAST PAGE IN THE ARRAY
		//--- WE DO THIS TO CREATE A SPECIAL CLASS
		$is_last = $pages_array;
		$is_last = array_pop($is_last);
		$is_last_class;
		//---- ALSO CHECK IF THE ARRAY IS BIGGER THAN 1
		//---- I THINK THIS WILL PREVENT FALSE POSITIVES
		if( $is_last == $post->ID && count($pages_array) > 1 ){
			$is_last_class = ' last-page';
		}

		// echo '<pre>';
		// print_r($pages_array);
		// echo '</pre>';
	?>

	<div class="wrap">

		<nav class="tabs-navigation<?php echo $is_last_class; ?>">
			<ul>
			<?php 
				wp_list_pages(array(
					"include" 		=> $ancestor_id, 
					"link_before" 	=> "", 
					"title_li" 	=> "", 
				 ));
				wp_list_pages(array(
					"child_of" 		=> $ancestor_id,
					"include" 		=> $incl, 
					"link_before"	=> "", 
					"title_li" 	=> "", 
					"sort_order" 	=> "ASC", 
					"sort_column"  => "menu_order",
				 ));
			?>
			</ul>
		</nav>


		<div class="primary content-area">

			<main id="main" class="site-main" role="main">

				<?php
				while ( have_posts() ) : the_post();

					get_template_part( 'template-parts/content', 'page' );

				endwhile; // End of the loop.
				?>


				<?php
					//FIND THE NEXT ITEM
					//IN THE PAGES ARRAY
					//AFTER THE CURRENT PAGE
					$curr_key = array_search($post->ID, $pages_array);
					$curr_key++;		
				?>

				<?php if( $curr_key  < count($pages_array) ){ ?> 
					<?php 
						$id = $pages_array[$curr_key];
						$title = get_the_title( $id ); 
						$post = get_post( $id );
					?>
					
					<div class="next-topic">
						<span>NEXT TOPIC:</span>
						<h3><?php echo $title; ?></h3>
						<p><?php echo wp_trim_words( $post->post_content ); ?></p>

						<a href="<?php echo get_permalink( $id ); ?>" class="btn"><?php echo $title; ?> &rarr;</a>
					</div>
				<?php } ?>

			</main><!-- #main -->
		</div><!-- .primary -->

		<?php get_sidebar(); ?>

	</div><!-- .wrap -->

<?php get_footer(); ?>