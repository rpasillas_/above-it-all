(function($, root, undefined){
	$(function () {
		'use strict';

			$(document).ready(function(){			
				// if you want to use the 'fire' or 'disable' fn,
				// you need to save OuiBounce to an object
				var _ouibounce = ouibounce(document.getElementById('ouibounce-modal'), {
					aggressive: false,
					timer: 0,
					sitewide: true
				//	callback: function() { console.log('ouibounce fired!'); }
				});

				$('#ouibounce-modal .underlay').on('click', function() {
					$('#ouibounce-modal').hide();
				});

				$('#ouibounce-modal .close').on('click', function() {
					$('#ouibounce-modal').hide();
				});

				$('#ouibounce-modal .modal').on('click', function(e) {
					e.stopPropagation();
				});			
			});

			$(document).on('mailsent.wpcf7', function () {
				console.log('close');
				$('#ouibounce-modal').find('.form-full, .close').hide(function(){
					setTimeout( function(){
						$('#ouibounce-modal').hide();
					}, 6000);
				});
			});

	});	
})(jQuery, this);