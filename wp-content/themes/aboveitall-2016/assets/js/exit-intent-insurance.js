(function($, root, undefined){
	$(function () {
		'use strict';

			$(document).ready(function(){			
				// if you want to use the 'fire' or 'disable' fn,
				// you need to save OuiBounce to an object
				var _ouibounce = ouibounce(document.getElementById('ouibounce-modal-insurance'), {
					aggressive: true,
					timer: 0,
					sitewide: true
				//	callback: function() { console.log('ouibounce fired!'); }
				});

				$('#ouibounce-modal-insurance .underlay').on('click', function() {
					$('#ouibounce-modal-insurance').hide();
				});

				$('#ouibounce-modal-insurance .close').on('click', function() {
					$('#ouibounce-modal-insurance').hide();
				});

				$('#ouibounce-modal-insurance .modal').on('click', function(e) {
					e.stopPropagation();
				});			
			});

			$(document).on('mailsent.wpcf7', function () {
				console.log('close');
				$('#ouibounce-modal-insurance').find('.form-full, .form-half, .close').hide(function(){
					setTimeout( function(){
						$('#ouibounce-modal-insurance').hide();
					}, 6000);
				});
			});

	});	
})(jQuery, this);