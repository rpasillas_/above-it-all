(function($, root, undefined){
	$(function () {
		'use strict';

		//TEMPORARY SUB NAVIGATION SIMULATION
		// $('#primary-menu').find('li.menu-item-has-children').first().addClass('on');
		// $('#primary-menu').find('li.menu-item-has-children').first().find('.sub-menu').find('.menu-item-has-children').last().addClass('on');


		$('.menu-item-has-children.menu-item-depth-0').on('click','.sub-trigger',function(){
			var $this = $(this),
				$parent = $this.parent(),
				$grandparent = $parent.parent();

			$grandparent.find('li.menu-item-depth-0').not( $parent ).removeClass('on');		
			$parent.toggleClass('on');
			
		});

	});	
})(jQuery, this);