(function($, root, undefined){
    $(function () {
        'use strict';

            $('.owl-carousel').owlCarousel({
                loop:true,
                margin:10,
                nav:false,
                dots: true,
                responsive:{
                    0:{
                        items:1
                    },                   
                }
            });

    }); 
})(jQuery, this);