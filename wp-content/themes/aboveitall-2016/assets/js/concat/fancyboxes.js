(function($, root, undefined){
	$(function () {
		'use strict';

		$('.fancybox-video').fancybox({
			openEffect  : 'none',
			closeEffect : 'none',
			helpers : {
				media : {}
			}
		});

		$('.fancybox-media').fancybox({
			openEffect  : 'none',
			closeEffect : 'none',
			helpers : {
				media : {}
			}
		});

	});	
})(jQuery, this);