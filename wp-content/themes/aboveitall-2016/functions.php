<?php
/**
 * Above it All functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Above it All
 */

if ( ! function_exists( 'aia_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function aia_setup() {
	/**
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Above it All, use a find and replace
	 * to change 'aia' to the name of your theme in all the template files.
	 * You will also need to update the Gulpfile with the new text domain
	 * and matching destination POT file.
	 */
	load_theme_textdomain( 'aia', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/**
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/**
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'aia' ),
		'primary-mobile' => esc_html__( 'Primary Mobile Menu', 'aia' ),
		'secondary' => esc_html__( 'Secondary Menu', 'aia' ),
		'footer' => esc_html__( 'Footer Menu', 'aia' ),
		'funnel-loved-ones' => 'Home Page Funnel: For Loved Ones',
		'funnel-myself' => 'Home Page Funnel: For Myself',
		'process-funnel-menu' => 'Scrolling Menu that appears on the rehab process funnel template.'
	) );

	/**
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'aia_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add styles to the post editor
	add_editor_style( array( 'editor-style.css', aia_font_url() ) );

}
endif; // aia_setup
add_action( 'after_setup_theme', 'aia_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function aia_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'aia_content_width', 640 );
}
add_action( 'after_setup_theme', 'aia_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function aia_widgets_init() {

	// Define sidebars
	$sidebars = array(
		'sidebar-1'  => esc_html__( 'Pages', 'aia' ),
		'sidebar-2'  => esc_html__( 'Blog', 'aia' ),
		'sidebar-3'  => esc_html__( 'Funnel Pages', 'aia' ),
	);

	// Loop through each sidebar and register
	foreach ( $sidebars as $sidebar_id => $sidebar_name ) {
		register_sidebar( array(
			'name'          => $sidebar_name,
			'id'            => $sidebar_id,
			'description'   => sprintf ( esc_html__( 'Widget area for %s', 'aia' ), $sidebar_name ),
			'before_widget' => '<aside class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		) );
	}

}
add_action( 'widgets_init', 'aia_widgets_init' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load styles and scripts.
 */
require get_template_directory() . '/inc/scripts.php';


require get_template_directory() . '/evaero-boilerplate/evaero-include.php';



add_action( 'wpcf7_mail_sent', 'my_conversion' );
function my_conversion( $contact_form )
{
	$title = $contact_form->title;
	$submission = WPCF7_Submission::get_instance();

	if ( $submission ) {
		$posted_data = $submission->get_posted_data();
	}

	$msg = ''; //a default since not all forms have this field

	switch( $title ){
		
		case 'Home':
			$first_name  = $posted_data["h-f-name"];
			$last_name  = $posted_data["h-l-name"];
			$email = $posted_data["h-email"];
			$phone = $posted_data["h-tel"];
			$msg = $posted_data["h-message"];
			break;
		case 'Footer':
			$first_name  = $posted_data["f-f-name"];
			$last_name  = $posted_data["f-l-name"];
			$email = $posted_data["f-email"];
			$phone = $posted_data["f-tel"];
			$msg = $posted_data["f-message"];
			break;
		case 'Sidebar':
			$first_name  = $posted_data["sb-f-name"];
			$last_name  = $posted_data["sb-l-name"];
			$email = $posted_data["sb-email"];
			$phone = $posted_data["sb-tel"];
			$msg = $posted_data["sb-msg"];
			break;	
		case 'Insurance Pop-Up':
			$first_name  = $posted_data["ins-pu-f-name"];
			$last_name  = $posted_data["ins-pu-l-name"];
			$email = $posted_data["ins-pu-email"];
			$phone = $posted_data["ins-pu-tel"];
			break;		
		case 'Pop-Up':
			$first_name  = $posted_data["pu-f-name"];
			$last_name  = $posted_data["pu-l-name"];
			$email = $posted_data["pu-email"];
			$phone = $posted_data["pu-phone"];
			$msg = $posted_data["pu-message"];
			break;	
		case 'Insurance Form':
			$first_name  = $posted_data["your-f-name"];
			$last_name  = $posted_data["your-l-name"];
			$email = $posted_data["your-email"];
			$phone = $posted_data["phone"];
			$msg = $posted_data["message"];
			break;		
		case 'Contact Form':
			$first_name  = $posted_data["c-f-name"];
			$last_name  = $posted_data["c-l-name"];
			$email = $posted_data["c-email"];
			$phone = $posted_data["c-tel"];
			$msg = $posted_data["c-message"];
			break;					
	}

	$post_items[] = 'oid=00D70000000N2A8EAK';
	$post_items[] = 'first_name=' . $first_name;
	$post_items[] = 'last_name=' . $last_name;
	$post_items[] = 'email=' . $email;
	$post_items[] = 'phone=' . $phone;
	$post_items[] = 'Additional_Information__c=' . $msg;
	$post_items[] = 'retURL=http://www.aboveitalltreatment.com';
	// $post_items[] = 'debugEmail=ron@evaero.co';
	// $post_items[] = 'debug=1';
	$post_items[] = 'external=1';
	$post_items[] = 'teravisiontech__Web_to_x_form_id__c=a0b7000000UfZaTAAV';
	$post_items[] = 'Campain_ID=701700000018kLSAAY';
	$post_items[] = 'lead_source=Website Form- AIA (Organic)';


	$post_string = implode ('&', $post_items);

	$ch = curl_init( 'https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8' ); 

	curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1 );
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
	
	curl_exec($ch); // Post to Salesforce
	curl_close($ch); // close cURL resource
}