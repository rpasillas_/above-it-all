<?php
/**
 * Custom functions that act independently of the theme templates.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Above it All
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function aia_body_classes( $classes ) {

	global $is_IE;

	// If it's IE, add a class.
	if ( $is_IE ) {
		$classes[] = 'ie';
	}

	// Give all pages a unique class.
	if ( is_page() ) {
		$classes[] = 'page-' . basename( get_permalink() );
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds "no-js" class. If JS is enabled, this will be replaced (by javascript) to "js".
	$classes[] = 'no-js';

	return $classes;
}
add_filter( 'body_class', 'aia_body_classes' );


//show all the staff members in a category
function staff_posts($query){
	if( !is_admin() && is_archive('staff') ){
		$query->set('posts_per_page', 100);
		return;
	}
}
add_action( 'pre_get_posts', 'staff_posts' );


function wpb_move_comment_field_to_bottom( $fields ) {
	$comment_field = $fields['comment'];
	unset( $fields['comment'] );
	unset( $fields['url'] );
	$fields['comment'] = $comment_field;
	return $fields;
}
add_filter( 'comment_form_fields', 'wpb_move_comment_field_to_bottom' );


//show all the staff members in a category
function stories_posts($query){
	if( !is_admin() && is_archive('stories') && $query->is_main_query() ){
		$query->set('orderby', 'title');
		return;
	}
}
add_action( 'pre_get_posts', 'stories_posts' );


//reorder press releases
function categories_posts($query){
	if( !is_admin() && is_archive('press-releases') && $query->is_main_query() ){
		$query->set('orderby', 'date');
		$query->set('order', 'DESC');
		return;
	}
}
add_action( 'pre_get_posts', 'categories_posts' );