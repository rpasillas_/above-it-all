<?php
/**
 * Above it All Theme Customizer.
 *
 * @package Above it All
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function aia_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	// Add our social link options.
    $wp_customize->add_section(
        'aia_social_links_section',
        array(
            'title'       => esc_html__( 'Social Links', 'aia' ),
            'description' => esc_html__( 'These are the settings for social links. Please limit the number of social links to 5.', 'aia' ),
            'priority'    => 90,
        )
    );

    // Create an array of our social links for ease of setup.
    $social_networks = array( 'twitter', 'facebook', 'youtube', 'googleplus', 'pinterest', 'linkedin' );

    // Loop through our networks to setup our fields.
    foreach( $social_networks as $network ) {

	    $wp_customize->add_setting(
	        'aia_' . $network . '_link',
	        array(
	            'default' => '',
	            'sanitize_callback' => 'aia_sanitize_customizer_url'
	        )
	    );
	    $wp_customize->add_control(
	        'aia_' . $network . '_link',
	        array(
	            'label'   => sprintf( esc_html__( '%s Link', 'aia' ), ucwords( $network ) ),
	            'section' => 'aia_social_links_section',
	            'type'    => 'text',
	        )
	    );
    }

    // Add our Footer Customization section section.
    $wp_customize->add_section(
        'aia_footer_section',
        array(
            'title'    => esc_html__( 'Footer Customization', 'aia' ),
            'priority' => 90,
        )
    );

    // Add our copyright text field.
    $wp_customize->add_setting(
        'aia_copyright_text',
        array(
            'default' => ''
        )
    );
    $wp_customize->add_control(
        'aia_copyright_text',
        array(
            'label'       => esc_html__( 'Copyright Text', 'aia' ),
            'description' => esc_html__( 'The copyright text will be displayed beneath the menu in the footer.', 'aia' ),
            'section'     => 'aia_footer_section',
            'type'        => 'text',
            'sanitize'    => 'html'
        )
    );
    // Add first part of our footer CTA
    $wp_customize->add_setting(
        'aia_cta_text_1',
        array(
            'default'           => ''
        )
    );
    $wp_customize->add_control(
        'aia_cta_text_1',
        array(
            'label'       => __( 'Footer CTA Line 1', 'aia' ),
            'description' => __( 'This first line of the CTA text will be displayed above the phone number in the footer.', 'aia' ),
            'section'     => 'aia_footer_section',
            'type'        => 'text',
            'sanitize'    => 'html'
        )
    );   

  // Add first part of our footer CTA
    $wp_customize->add_setting(
        'aia_cta_text_2',
        array(
            'default'           => ''
        )
    );
    $wp_customize->add_control(
        'aia_cta_text_2',
        array(
            'label'       => __( 'Footer CTA Line 2', 'aia' ),
            'description' => __( 'This second line of the CTA text will be displayed above the phone number in the footer.', 'aia' ),
            'section'     => 'aia_footer_section',
            'type'        => 'text',
            'sanitize'    => 'html'
        )
    );   

    // Add our footer CTA Phone Message
    $wp_customize->add_setting(
        'aia_cta_phone_text',
        array(
            'default'           => ''
        )
    );
    $wp_customize->add_control(
        'aia_cta_phone_text',
        array(
            'label'       => __( 'Footer CTA Phone Message', 'aia' ),
            'description' => __( 'The text will be displayed beneath the phone number in the footer. (Toll-Free, etc.)', 'aia' ),
            'section'     => 'aia_footer_section',
            'type'        => 'text',
            'sanitize'    => 'html'
        )
    ); 

    // Add our Contact Form Section Customization section.
    $wp_customize->add_section(
        'aia_contact_section',
        array(
            'title'    => esc_html__( 'Contact Form Section Customization', 'aia' ),
            'priority' => 90,
        )
    );

    // Add our copyright text field.
    $wp_customize->add_setting(
        'aia_contact_section_text',
        array(
            'default' => ''
        )
    );
    $wp_customize->add_control(
        'aia_contact_section_text',
        array(
            'label'       => esc_html__( 'Contact Form Section Text', 'aia' ),
            'description' => esc_html__( 'The text that will be displayed next to the contact form section.', 'aia' ),
            'section'     => 'aia_contact_section',
            'type'        => 'textarea',
            'sanitize'    => 'html'
        )
    );

    // Add our Contact Form Section Customization section.
    $wp_customize->add_section(
        'aia_testimonials_section',
        array(
            'title'    => esc_html__( 'Testimonials Form Section Customization', 'aia' ),
            'priority' => 90,
        )
    );

    // Add our copyright text field.
    $wp_customize->add_setting(
        'aia_testimonials_section_title',
        array(
            'default' => ''
        )
    );
    $wp_customize->add_control(
        'aia_testimonials_section_title',
        array(
            'label'       => esc_html__( 'Testimonials Form Section Title', 'aia' ),
            'description' => esc_html__( 'The title that will be displayed next to the testimonials form section.', 'aia' ),
            'section'     => 'aia_testimonials_section',
            'type'        => 'text',
            'sanitize'    => 'html'
        )
    );
    $wp_customize->add_setting(
        'aia_testimonials_section_text',
        array(
            'default' => ''
        )
    );
    $wp_customize->add_control(
        'aia_testimonials_section_text',
        array(
            'label'       => esc_html__( 'Testimonials Form Section Text', 'aia' ),
            'description' => esc_html__( 'The text that will be displayed next to the testimonials form section.', 'aia' ),
            'section'     => 'aia_testimonials_section',
            'type'        => 'textarea',
            'sanitize'    => 'html'
        )
    );

    $wp_customize->add_setting(
        'aia_testimonials_video_thumb',
        array(
            'default' => ''
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'aia_testimonials_video_thumb',
            array(
                'label'    => 'Video Thumbnail Image',
                'settings' => 'aia_testimonials_video_thumb',
                'section'  => 'aia_testimonials_section'
            )
        )
    );

    $wp_customize->add_setting(
        'aia_testimonials_video_title',
        array(
            'default' => ''
        )
    );
    $wp_customize->add_control(
        'aia_testimonials_video_title',
        array(
            'label'       => esc_html__( 'Video Title', 'aia' ),
            'description' => esc_html__( '', 'aia' ),
            'section'     => 'aia_testimonials_section',
            'type'        => 'text',
            'sanitize'    => 'html'
        )
    );
    $wp_customize->add_setting(
        'aia_testimonials_video_description',
        array(
            'default' => ''
        )
    );
    $wp_customize->add_control(
        'aia_testimonials_video_description',
        array(
            'label'       => esc_html__( 'Video Description', 'aia' ),
            'description' => esc_html__( '', 'aia' ),
            'section'     => 'aia_testimonials_section',
            'type'        => 'textarea',
            'sanitize'    => 'html'
        )
    );

    $wp_customize->add_setting(
        'aia_testimonials_video_url',
        array(
            'default' => ''
        )
    );
    $wp_customize->add_control(
        'aia_testimonials_video_url',
        array(
            'label'       => esc_html__( 'YouTube URL', 'aia' ),
            'description' => esc_html__( '', 'aia' ),
            'section'     => 'aia_testimonials_section',
            'type'        => 'text',
            'sanitize'    => 'html'
        )
    );

    $wp_customize->add_setting(
        'aia_testimonials_review_title',
        array(
            'default' => ''
        )
    );
    $wp_customize->add_control(
        'aia_testimonials_review_title',
        array(
            'label'       => esc_html__( 'Reputation Force Title', 'aia' ),
            'description' => esc_html__( '', 'aia' ),
            'section'     => 'aia_testimonials_section',
            'type'        => 'text',
            'sanitize'    => 'html'
        )
    );
    $wp_customize->add_setting(
        'aia_testimonials_review_description',
        array(
            'default' => ''
        )
    );
    $wp_customize->add_control(
        'aia_testimonials_review_description',
        array(
            'label'       => esc_html__( 'Reputation Force Description', 'aia' ),
            'description' => esc_html__( '', 'aia' ),
            'section'     => 'aia_testimonials_section',
            'type'        => 'textarea',
            'sanitize'    => 'html'
        )
    );

    //home page funnel images
    $wp_customize->add_section(
        'aia_funnel_section',
        array(
            'title'    => esc_html__( 'Home Page Funnel Section Customization', 'aia' ),
            'priority' => 90,
        )
    );
    $wp_customize->add_setting(
        'aia_funnel_loved_one_thumb',
        array(
            'default' => ''
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'aia_funnel_loved_one_thumb',
            array(
                'label'    => 'Funnel Image: For Loved Ones',
                'settings' => 'aia_funnel_loved_one_thumb',
                'section'  => 'aia_funnel_section'
            )
        )
    );
    $wp_customize->add_setting(
        'aia_funnel_myself_thumb',
        array(
            'default' => ''
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'aia_funnel_myself_thumb',
            array(
                'label'    => 'Funnel Image: For Loved Ones',
                'settings' => 'aia_funnel_myself_thumb',
                'section'  => 'aia_funnel_section'
            )
        )
    );
    
}
add_action( 'customize_register', 'aia_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function aia_customize_preview_js() {
    wp_enqueue_script( 'aia_customizer', get_template_directory_uri() . '/assets/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'aia_customize_preview_js' );

/**
 * Sanitize our customizer text inputs.
 */
function aia_sanitize_customizer_text( $input ) {
    return sanitize_text_field( force_balance_tags( $input ) );
}

/**
 * Sanitize our customizer URL inputs.
 */
function aia_sanitize_customizer_url( $input ) {
    return esc_url( $input );
}
