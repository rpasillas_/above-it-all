<?php
/**
 * Custom scripts and styles.
 *
 * @package Above it All
 */

/**
 * Register Google font.
 *
 * @link http://themeshaper.com/2014/08/13/how-to-add-google-fonts-to-wordpress-themes/
 */
function aia_font_url() {

	$fonts_url = '';

	/**
	 * Translators: If there are characters in your language that are not
	 * supported by the following, translate this to 'off'. Do not translate
	 * into your own language.
	 */
	$libre_baskerville = _x( 'on', 'Libre Baskerville font: on or off', 'aia' );
	$raleway = _x( 'on', 'Raleway font: on or off', 'aia' );

	if ( 'off' !== $libre_baskerville || 'off' !== $raleway ) {
		$font_families = array();

		if ( 'off' !== $libre_baskerville ) {
			$font_families[] = 'Libre Baskerville:400,700';
		}

		if ( 'off' !== $raleway ) {
			$font_families[] = 'Raleway:400,700';
		}

		$query_args = array(
			'family' => urlencode( implode( '|', $font_families ) ),
		);

		$fonts_url = add_query_arg( $query_args, '//fonts.googleapis.com/css' );
	}

	return $fonts_url;
}

/**
 * Enqueue scripts and styles.
 */
function aia_scripts() {
	/**
	 * If WP is in script debug, or we pass ?script_debug in a URL - set debug to true.
	 */
	$debug = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG == true ) || ( isset( $_GET['script_debug'] ) ) ? true : false;

	/**
	 * If we are debugging the site, use a unique version every page load so as to ensure no cache issues.
	 */
	$version = '1.0.0';

	/**
	 * Should we load minified files?
	 */
	$suffix = ( true === $debug ) ? '' : '.min';

	// Register styles.
	wp_register_style( 'aia-google-font', aia_font_url(), array(), null );

	// Enqueue styles.
	wp_enqueue_style( 'aia-google-font' );
	wp_enqueue_style( 'animate.css' );
	wp_enqueue_style( 'aia-style', get_stylesheet_directory_uri() . '/style' . $suffix . '.css', array(), $version );

	// Enqueue scripts.
	wp_enqueue_script( 'aia-scripts', get_template_directory_uri() . '/assets/js/project' . $suffix . '.js', array( 'jquery' ), $version, true );
	wp_enqueue_script( 'aia-fancybox-helpers', get_template_directory_uri() . '/assets/js/fancybox-helpers/jquery.fancybox-media.js', array( 'jquery' ), $version, true );

	//scroll down to end of this file
	//to see the rest of this script
	wp_enqueue_script( 'adobedtm', '//assets.adobedtm.com/c876840ac68fc41c08a580a3fb1869c51ca83380/satelliteLib-48a2d96f58bc6dbdc3cb3eb066e18c4d934a987b.js', '', null, false );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if( is_front_page() ){
		wp_enqueue_script( 'vide', get_template_directory_uri() . '/assets/bower_components/vide/dist/jquery.vide.min.js', array( 'jquery' ), $version, true );
	}
	
}
add_action( 'wp_enqueue_scripts', 'aia_scripts' );


if ( class_exists( 'WDS_Simple_Page_Builder' ) && version_compare( WDS_Simple_Page_Builder::VERSION, '1.6', '>=' ) ) :

	/**
	 * Conditionally enqueue styles & scripts via Page Builder.
	 */
	function aia_enqueue_page_builder_scripts() {

		// Get the page builder parts
		$parts = get_page_builder_parts();

		// // If page builder part exsists, enqueue script
		// if ( in_array( 'cover-flow' , $parts ) ) {
		// 	wp_register_script( 'cover-flow', get_stylesheet_directory_uri() . '/js/cover-flow-script.js', array(), $version, true );
		// 	wp_enqueue_script( 'cover-flow' );
		// }

	}
	add_action( 'wds_page_builder_after_load_parts', 'aia_enqueue_page_builder_scripts' );

endif;

/**
 * Add SVG definitions to <head>.
 */
function aia_include_svg_icons() {

	// Define SVG sprite file.
	$svg_icons = get_template_directory() . '/assets/images/svg-icons.svg';

	// If it exsists, include it.
	if ( file_exists( $svg_icons ) ) {
		require_once( $svg_icons );
	}
}




function staff_script(){
	if( is_archive( 'archive-staff' ) ){
?>
	<script>
		(function($, root, undefined){
			$(function () {
				'use strict';
				function staffBio(id, img, creds){
					var url = '/wp-json/wp/v2/staff/' + id;
					var content;

					

					$.getJSON( url )
						.done( function( json ){

							content = '<div class="thumb-wrap"><img src="' + img + '" /></div><div class="content">';
							content += ( json.title.rendered ) ? '<strong>' + json.title.rendered + '</strong><br>' : '';
							content += ( creds ) ? '<strong>' + creds + '</strong><br>' : '';
							content += '<p>' + json.content.rendered + '</p></div>';
							
							$.fancybox.open({ 
								maxWidth: 800,
								maxHeight: 550,
								padding: [0,0,0,0],
								autoSize: false,
								content: content, 
							});
							
						})
						.fail( function( jqxhr, textStatus, error ){
							var err = textStatus + ', ' + error;
							console.log( 'Request Failed: ' + err );
						});
				}

				$('.staff-member.has-bio').on('click', function(e){
					e.preventDefault();
					var $el = $(this),
						id = $el.data('id'),
						img = $el.data('img'),
						creds = $el.data('creds');

					staffBio(id, img, creds);
				});
			});	
		})(jQuery, this);
	</script>
<?php		
	}
}
add_action('wp_footer', 'staff_script');



function adobedtm(){
	echo '<script type="text/javascript">_satellite.pageBottom();</script>';
}
add_action('wp_footer', 'adobedtm');




function processTemplateScript(){
	if( is_page_template( 'page-treatment-process.php' ) ){	
		global $post;
		$current_page = $post->ID;
		$i = 0;

		//get the process template menu
		$child_pages = wp_get_nav_menu_items(105);

		
		//loop through all the pages in the menu
		//to find which is the current page
		//pass that page to the owl carousel
		//start position
		if ( $child_pages ) : 
			foreach ( $child_pages as $pageChild ) : 
				$id = get_post_meta( $pageChild->ID, '_menu_item_object_id', true );

				if($id == $current_page){
					$the_one = $i;
				}
				$i++;
			endforeach;
		endif;

		// echo '<pre>';
		// var_dump($the_one);
		// echo '</pre>'; 
?>
	<script>
		(function($, root, undefined){
			$(function () {
				'use strict';

				$('.tabs-navigation').find('ul').owlCarousel({
					loop: false,
					margin: 0,
					nav: true,
					stagePadding: 70,
					URLhashListener: true,
					startPosition: <?php echo $the_one; ?>,
					responsive:{
						0: {
							items: 1
						},
						600:{
							items: 3
						}
					}

				});

			});	
		})(jQuery, this);
	</script>

<?php
	}
}
add_action('wp_footer', 'processTemplateScript');



function expenseForm(){
	if( is_page(3477) ){
?>
	<script type="text/javascript">
		function doSubmit(thisform){
			with(thisform) { 
				var error = "";
				var amtsel = false;
				//All required fields need to be filled
				if(cardholder_first_name.value == "" || cardholder_first_name.value == null)
				error = error + "Please fill in the Cardholder First Name.\n ";
				if(cardholder_last_name.value == "" || cardholder_last_name.value == null)
				error = error + "Please fill in the Cardholder Last Name.\n ";
				if(cardholder_phone.value == "" || cardholder_phone.value == null)
				error = error + "Please fill in the Cardholder Phone Number.\n ";
				if(customer_email.value == "" || customer_email.value == null)
				error = error + "Please fill in the Cardholder Email field.\n ";
				if(ship_to_first_name.value == "" || ship_to_first_name.value == null)
				error = error + "Please fill in the Patient First Name.\n ";
				if(ship_to_last_name.value == "" || ship_to_last_name.value == null)
				error = error + "Please fill in the Patient Last Name.\n ";
				if(isNaN(prefee.value) || prefee.value == "" || prefee.value == null)
				error = error + "Invalid dollar amount.\n ";
				transaction_amount.value = amount.value; 

				if(error != "") {
					alert(error);
				return false;
				}
				else {
					subbut.disabled = true;
					return true;
				}
			}
		}

		function roundNumber(num, dec) {
			var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
			return result;
		} 
		function calculate() {
	        var userinput = document.pettycash.prefee.value,
	            result = (userinput) * 1.05;
	            result = result.toFixed(2);
	        document.pettycash.amount.value = result;
	    }
	    jQuery('#prefee').change(function(){
	    	calculate();
	    });

	    jQuery('#pettyCash').submit(function(){
	    	return doSubmit(this);
	    });
	</script>

	<style type="text/css" media="screen">
		 #payhereTable th { border: 1px solid #666666; background-color: #AAAAAA; }
		 .req { color: red; }
	</style>
	
<?php
	}
}
add_action('wp_footer', 'expenseForm');
?>