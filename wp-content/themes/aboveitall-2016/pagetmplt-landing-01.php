<?php
	/* Template Name: Landing Page #1 */
?>

<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#" lang="en">
	<head>
		<!-- Google Analytics Content Experiment code -->
			<script>function utmx_section(){}function utmx(){}(function(){var
			k='40622962-3',d=document,l=d.location,c=d.cookie;
			if(l.search.indexOf('utm_expid='+k)>0)return;
			function f(n){if(c){var i=c.indexOf(n+'=');if(i>-1){var j=c.
			indexOf(';',i);return escape(c.substring(i+n.length+1,j<0?c.
			length:j))}}}var x=f('__utmx'),xx=f('__utmxx'),h=l.hash;d.write(
			'<sc'+'ript src="'+'http'+(l.protocol=='https:'?'s://ssl':
			'://www')+'.google-analytics.com/ga_exp.js?'+'utmxkey='+k+
			'&utmx='+(x?x:'')+'&utmxx='+(xx?xx:'')+'&utmxtime='+new Date().
			valueOf()+(h?'&utmxhash='+escape(h.substr(1)):'')+
			'" type="text/javascript" charset="utf-8"><\/sc'+'ript>')})();
			</script><script>utmx('url','A/B');</script>
		<!-- End of Google Analytics Content Experiment code -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=688">
		<link rel="shortcut icon" href="favicon.ico">

		<title><?php wp_title(); ?></title>

		<meta prefix="og: http://ogp.me/ns#" property="og:title" content="<?php echo get_the_title(); ?>"/>
		<meta prefix="og: http://ogp.me/ns#" property="og:image" content="<?php thumb_url(); ?>"/>
		<link rel="image_src" href="<?php thumb_url(); ?>" />

		<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/style-landing.css" rel="stylesheet">

		<?php if ( ot_get_option( 'ppc_google_analytics' ) ) echo '<script type="text/javascript">' . "\n" . ot_get_option( 'ppc_google_analytics' ) . "\n\t\t" . '</script>'; ?>
		<?php if ( ot_get_option( 'ppc_google_remarketing' ) ) echo ot_get_option( 'ppc_google_remarketing' ); ?>
		<?php if ( ot_get_option( 'ppc_infinity' ) ) echo ot_get_option( 'ppc_infinity' ); ?>

		<script async src="//5239.tctm.co/t.js"></script>

		<?php wp_head(); ?>
	</head>

	<body <?php body_class(); ?>>

		<nav>
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<a href="/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/landing/logo-01.png" /></a>
					</div>
					<div class="col-md-9">
						<span class="call"><span class="first">Have Questions?</span> Call Us at <?php echo do_shortcode('[phone]'); ?></span>
					</div>
				</div>
			</div>
		</nav>

		<header>
			<div class="container">
				<div class="callout">
					<div class="pre-form toggle">
						<span class="h1">Naturally Effective Recovery</span>
						<span class="go-form desktop-only"><span>Contact Now</span> for total recovery from Drug &amp; Alcohol Addiction</span>
						<span class="go-form mobile-only"><span>Contact Now</span> for total recovery from Drug &amp; Alcohol Addiction</span>
						<p>Above It All Treatment, located in the beautiful, naturally healing environment of Lake Arrowhead, California, helps clients with drug and alcohol addictions.</p>
					</div>
					<div id="form" class="form toggle">
						<p class="instructions">Enter your information to have an advisor contact you.</p>
						<?php echo do_shortcode('[contact-form-7 id="3608" title="Landing Page #1"]'); ?>
					</div>
					<div id="thanks" class="thanks">
						<p>Thank you for contacting us! One of our advisors will be in touch with you shortly.</p>
					</div>
				</div>
			</div>
		</header>

		<footer>
			<div class="mountains">
				<div class="container">
					<div class="col-md-12">
						<span class="title"><?php echo ot_get_option( 'footer_mountains_title' ); ?></span>
						<p class="content-top"><?php echo ot_get_option( 'footer_mountains_content_top' ); ?></p>
					</div>
					<div class="col-md-6">
						<p class="content-bottom"><?php echo ot_get_option( 'footer_mountains_content_bottom' ); ?></p>
						<a class="get-help desktop-only" href="<?php echo get_permalink(get_page_by_path('contact')); ?>">Change Your Life <span>Call Today</span></a>
						<a class="get-help mobile-only" href="tel:<?php echo ot_get_option('phone'); ?>">Change Your Life <span>Call Today</span></a>
					</div>
					<div class="col-md-6">
					</div>
				</div>
			</div>
			<div class="bottom">
				<div class="container">
					<div class="col-md-6 social">
						<p><span>Get Social With AIA</span></p>
						<ul>
							<li><a href="https://www.facebook.com/<?php echo ot_get_option( 'social_facebook' ); ?>" target="_blank" class="facebook">Facebook</a></li>
							<li><a href="http://www.twitter.com/<?php echo ot_get_option( 'social_twitter' ); ?>" target="_blank" class="twitter">Twitter</a></li>
							<li><a href="https://plus.google.com/<?php echo ot_get_option( 'social_googleplus' ); ?>" target="_blank" class="gplus">Google+</a></li>
							<li><a href="https://www.pinterest.com/<?php echo ot_get_option( 'social_pinterest' ); ?>" target="_blank" class="pinterest">Pinterest</a></li>
							<li><a href="http://www.youtube.com/user/<?php echo ot_get_option( 'social_youtube' ); ?>" target="_blank" class="youtube">YouTube</a></li>
							<li><a href="https://linkedin.com/company/<?php echo ot_get_option( 'social_linkedin' ); ?>" target="_blank" class="linkedin">LinkedIn</a></li>
						</ul>
					</div>
					<div class="col-md-6 hotline">
						<span class="call-today">Have Questions? <?php echo do_shortcode('[phone]'); ?></span>
					</div>
				</div>
			</div>
		</footer>

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/bootstrap.min.js"></script>
		<script>
			$( ".go-form" ).click(function() {
				$( ".toggle" ).toggle();
			});
		</script>

		<?php wp_footer(); ?>
	</body>
</html>