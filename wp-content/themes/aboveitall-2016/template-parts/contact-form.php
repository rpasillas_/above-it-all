<div class="hero-contact-form">
	<div class="wrap">
		<div class="cta-column">
			<svg class="logo"><use xlink:href="#icon-aia-logo-white"></use></svg>
			<span class="h3"><?php echo get_theme_mod('aia_contact_section_text'); ?></span>
			
			<svg class="phone-icon"><use xlink:href="#icon-phone"></use></svg>
			<?php echo do_shortcode('[phone]'); ?>
			<svg class="swoop-arrow"><use xlink:href="#icon-swoop-arrow"></use></svg>
		</div>
		<div class="form-column">
			<?php echo do_shortcode( '[contact-form-7 id="4607" title="Footer"]' ); ?>
		</div>
	</div>
</div>