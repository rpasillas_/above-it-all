<div class="hero-insurance">
	<div class="wrap">
		<div class="insurance-providers">
			<span class="h4"><a href="<?php echo get_permalink(204); ?>">Most Insurance Accepted</a></span>
			<a href="<?php echo get_permalink(204); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/united-healthcare.png"></a>
			<a href="<?php echo get_permalink(204); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/blue-cross-blue-shield.png"></a>
			<a href="<?php echo get_permalink(204); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/aetna.png"></a>
		</div>
		<div class="standards-practices">
			<span class="h4"><a href="<?php echo get_permalink(231); ?>">Standards &amp; Best Practices</a></span>
			<a href="<?php echo get_permalink(231); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/carf.png"></a>
			<a href="<?php echo get_permalink(231); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/cdhcs.png"></a>
			<a href="<?php echo get_permalink(231); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/naatp.png"></a>
		</div>
	</div>
</div>