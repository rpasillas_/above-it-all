<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Above it All
 */

?>

<article <?php post_class(); ?> data-template="content-page">
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<?php if( is_page(3477) ){ ?>
		<div class="content-nav">Jump To: <a href="#pettyCash">Client Expense Account Form</a> <span>|</span> <a href="#delivery">Delivery of Letters &amp; Packages</a></div> 
	<?php } ?>

	<div class="entry-content">
		<?php 
			$hasImage = false;
			if( has_post_thumbnail() ){
				echo '<figure class="post-thumb-wrapper">';
					the_post_thumbnail('medium'); 
					echo '<figcaption>Call Today: ' . do_shortcode('[phone]') . '</figcaption>';
				echo '</figure>';

				$hasImage = true;
			}			
		?>



		<?php
			if( is_page(4175) ){ ?>
							<iframe src="https://www.reputationforce.net/bz/embed-above-it-all-treatment-center" height="850" width="100%"></iframe> <script type="text/javascript" src="https://www.reputationforce.net/bz/assets/api/reviews/reputation_force.min.js"></script>

	<?php	} 
			else{

				if(!$hasImage){
					$class = ' full-width';	
				}else{
					$class = '';
				}
			?>
				<div class="page-share-wrap<?php echo $class; ?>"><div class="page-share-wrap-inner"><span>Share Page</span> <?php echo do_shortcode('[ssba]'); ?></div></div>
			<?php
				the_content();
			}
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					esc_html__( 'Edit %s', 'aia' ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->