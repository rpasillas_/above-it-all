<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Above it All
 */

?>

<article <?php post_class(); ?> data-template="content">
	<header class="entry-header">
		<?php
			if ( !is_single() ) : ?>
			<div class="entry-thumb">
				<?php the_post_thumbnail(); ?>
			</div>

			<div class="entry-meta">
				<?php aia_entry_footer(); ?>
			</div><!-- .entry-meta -->
			
			<?php
			endif;


			if ( is_single() ) {
				the_title( '<h1 class="entry-title">', '</h1>' );
			} else {
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			}

		if ( is_single() ) : ?>
		<div class="entry-meta">
			<?php aia_entry_footer(); ?>

			<div class="share-wrap"><span>Share Page</span> <?php echo do_shortcode('[ssba]'); ?></div>
		</div><!-- .entry-meta -->
		<?php endif; ?>

	</header><!-- .entry-header -->

<?php if ( is_single() ) { ?>
	
	<div class="entry-content">

		<?php 
			if( has_post_thumbnail() ){
				echo '<figure class="post-thumb-wrapper">';
					the_post_thumbnail('medium'); 
					echo '<figcaption>Call Today: ' . do_shortcode('[phone]') . '</figcaption>';
				echo '</figure>';
			}			
		?>

		<?php
			the_content( sprintf(
				/* translators: %s: Name of current post. */
				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'aia' ), array( 'span' => array( 'class' => array() ) ) ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'aia' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

<?php } ?>

</article><!-- #post-## -->