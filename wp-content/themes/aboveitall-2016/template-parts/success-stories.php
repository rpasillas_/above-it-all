<?php
	$quotes;

	for( $i=1; $i<4; $i++){
		$quotes .= '<div class="item">' . of_get_option( 'testimonial_' . $i ) . '</div>';
	}


?>
<div class="hero-success-stories">
	<div class="wrap">
		<div class="preamble">
			<div class="inner-preamble">
				<span class="h2"><?php echo get_theme_mod('aia_testimonials_section_title'); ?></span>
				<div class="logo-separator">
					<svg><use xlink:href="#icon-aia-logo-mountain"></use></svg>
				</div>
				<p><?php echo get_theme_mod('aia_testimonials_section_text'); ?></p>
			</div>
		</div>

		<div class="success-item quotes-testimonial" id="quotes-carousel">
			<svg class="quotes"><use xlink:href="#icon-large-quotes"></use></svg>
			<div class="owl-carousel">
				<?php echo $quotes; ?>	
			</div>		
		</div>
		
		<div class="success-item video-testimonial">
			<a href="<?php echo get_theme_mod('aia_testimonials_video_url'); ?>" class="fancybox-video"><img src="<?php echo get_theme_mod('aia_testimonials_video_thumb'); ?>"></a>
			<span class="h3"><?php echo get_theme_mod('aia_testimonials_video_title'); ?></span>
			<p><?php echo get_theme_mod('aia_testimonials_video_description'); ?></p>
		</div>

		<div class="success-item reviews-testimonial">
			<div id="rfwidget_container"></div><script type="text/javascript">var bn='Above It All Treatment Center';var accesToken='RkklrdfzaSyBm8GGPcUi9gFovcqfPyHSvROGpqwwe';var reviewLink='/community/leave-a-review/';</script><script type="text/javascript" src="https://www.reputationforce.net/bz/assets/api/widget/RF.Widget.js"></script>
			<span class="h3"><?php echo get_theme_mod('aia_testimonials_review_title'); ?></span>
			<p><?php echo get_theme_mod('aia_testimonials_review_description'); ?></p>
		</div>

		<div class="btn-wrap"><a href="<?php echo get_permalink(47); ?>" class="btn">View More Testimonials</a></div>

	</div>
</div>