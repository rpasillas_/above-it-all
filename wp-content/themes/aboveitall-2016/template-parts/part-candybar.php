<div class="cta-banner">
	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/hand-grasp-man-climbing.png" alt="Men grasping hands to help a climber.">
	<div>
		<span class="h3">Ready for Help? Call Us Today.</span>
		<span>Our treatment specialists can guide you through a free, quick and confidential assessment</span>
		<?php echo do_shortcode( '[phone_alt]' ); ?>
	</div>
</div>