<div class="tour">
	<a href="<?php echo get_permalink(85); ?>" class="btn">Tour Above It All</a>

	<div class="gallery-image">
		<img src="<?php echo get_field('tour_gallery_image_1', 8 ); ?>">
	</div>	
	<div class="desktop gallery-image">
		<img src="<?php echo get_field('tour_gallery_image_5', 8 ); ?>">
	</div>
	<div class="desktop gallery-image">
		<img src="<?php echo get_field('tour_gallery_image_6', 8 ); ?>">
	</div>
	<div class="desktop gallery-image">
		<img src="<?php echo get_field('tour_gallery_image_7', 8 ); ?>">
	</div>
	<div class="gallery-image">
		<img src="<?php echo get_field('tour_gallery_image_3', 8 ); ?>">
	</div>
	<!--end of row -->

	<div class="gallery-image">
		<img src="<?php echo get_field('tour_gallery_image_2', 8 ); ?>">
	</div>
	<div class="desktop gallery-image">
		<img src="<?php echo get_field('tour_gallery_image_8', 8 ); ?>">
	</div>	
	<div class="desktop gallery-image">
		<img src="<?php echo get_field('tour_gallery_image_9', 8 ); ?>">
	</div>
	<div class="desktop gallery-image">
		<img src="<?php echo get_field('tour_gallery_image_10', 8 ); ?>">
	</div>		
	<div class="gallery-image">
		<img src="<?php echo get_field('tour_gallery_image_4', 8 ); ?>">
	</div>
	<!--end of row -->
</div>