<div class="page-banner-home" style="background-image: url( '<?php echo get_custom_header()->url; ?>' );">
	
	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/aia-logo-alt.svg">
	<span class="h4"><?php echo get_bloginfo( 'description' ); ?></span>

	
	<a href="<?php echo get_permalink(41); ?>" class="btn">Contact Us<svg class="swoop-arrow-alt"><use xlink:href="#icon-swoop-arrow-alt"></use></svg></a>
	<a href="<?php echo get_permalink(85); ?>" class="btn">View Our Facility</a>
	

	<!-- 532px -->
	<div class="bg-video-wrap">
		<div style="width: 100%; height: 532px;"
	  data-vide-bg="mp4: <?php echo get_template_directory_uri(); ?>/assets/video/AIA_Hero_Video_Final_mst.mp4, poster: <?php echo get_custom_header()->url; ?>" data-vide-options="posterType: jpg, loop: true, muted: false, position: 0% 0%"  >
		</div>
	</div>

	
</div>