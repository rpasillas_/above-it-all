<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Above it All
 */
?>

<aside class="secondary widget-area" role="complementary">
	<?php if (  is_active_sidebar( 'sidebar-1' ) && is_page() && !is_page(3477) && !is_page_template( 'page-treatment-funnel.php' ) && !is_page_template( 'page-treatment-process.php' ) ) : ?>

		<?php dynamic_sidebar( 'sidebar-1' ); ?>

	<?php elseif( is_active_sidebar( 'sidebar-2' ) && is_page(3477) ) : ?>

		<?php dynamic_sidebar( 'sidebar-2' ); ?>

	<?php elseif( is_active_sidebar( 'sidebar-2' ) && !is_page() ) : ?>

		<?php dynamic_sidebar( 'sidebar-2' ); ?>

		<div class="fb-page" data-href="https://www.facebook.com/aboveitalltreatmentcenter" data-tabs="timeline" data-width="358" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/aboveitalltreatmentcenter"><a href="https://www.facebook.com/aboveitalltreatmentcenter">Above It All Treatment Center</a></blockquote></div></div>

	<?php elseif (  is_active_sidebar( 'sidebar-3' ) && is_page() ) : ?>
		
		<?php dynamic_sidebar( 'sidebar-3' ); ?>

	<?php endif; ?>
</aside><!-- .secondary -->