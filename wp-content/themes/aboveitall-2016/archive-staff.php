<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Above it All
 */

get_header(); ?>

	<div class="wrap">

		<div class="primary content-area">
			<main class="site-main">
			<header>
				<h1>Meet Our Staff</h1>
			</header>

			<nav class="staff-nav">
				<ul>
					<li class="on"><a href="<?php echo get_term_link(94); ?>">Executive Staff</a></li>
					<li><a href="<?php echo get_term_link(95); ?>">Management Staff</a></li>
					<li><a href="<?php echo get_term_link(96); ?>">Therapists</a></li>
					<li><a href="<?php echo get_term_link(97); ?>">Counselors</a></li>
				</ul>
			</nav>

			<?php
				$args = array(
							'post_type' => 'staff',
							'tax_query' => array(
												array(
													'taxonomy' => 'staff_cat',
													'field'	=> 'slug',
													'terms' => 'executive-staff'
												)
											),
							'order' => 'DESC',
							'orderby' => 'menu_order',
							'posts_per_page' => -1
						);

				$senior_staff = new WP_Query( $args );
			?>

			<?php if ( $senior_staff->have_posts() ) : ?>

				<?php while ( $senior_staff->have_posts() ) : $senior_staff->the_post(); ?>
					
					<?php
						//Set Up The Image
						$thumb_id = get_post_thumbnail_id();
						$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'staff-roll-thumb', true);
						$thumb_url = $thumb_url_array[0];
					?>

					
					<div class="staff-member <?php if( get_the_content() !== '' ){ echo 'has-bio'; } ?>" data-id="<?php the_id(); ?>" data-creds="<?php the_field('staff-credentials'); ?>" data-title="<?php the_field('staff-title'); ?>"  data-img="<?php the_field('staff_candid_image'); ?>">

						
						<?php if ( has_post_thumbnail() ) { the_post_thumbnail('staff-roll-thumb'); } ?>
	

						<div class="staff-info">
							<span class="staff-name"><?php the_title(); ?></span>
							<!-- .staff-title -->
	
							<?php 
								if( get_field('staff_credentials') ) {  
									echo '<span class="staff-credentials">' . get_field('staff_credentials') . '</span>';
								}
							?>	
							
							<?php 
								if( get_field('staff_title') ) {  
									echo '<span class="staff-title">' . get_field('staff_title') . '</span>';
								}
							?>
						</div>
						<div class="hover">View Bio</div>

					</div>




				<?php endwhile; ?>

			<?php else : ?>

				<?php get_template_part( 'template-parts/content', 'none' ); ?>

			<?php endif; ?>

			<?php wp_reset_query(); ?>

			</main><!-- #main -->
		</div><!-- .primary -->

		<?php get_sidebar(); ?>

	</div><!-- .wrap -->

<?php get_footer(); ?>
