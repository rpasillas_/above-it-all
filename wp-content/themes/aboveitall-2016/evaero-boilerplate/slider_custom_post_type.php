<?php
/**
 * Custom Post type for use by the home page slideshow.
 *
 * @package WordPress
 * @subpackage evaero-boilerplate
 * @since Evaero Boilerplate 1.0
 *
 */
function slider_post_type() {
	register_post_type( 'slider_type',

		array( 'labels' => array(
			'name' => 'Homepage Slider',
			'singular_name' => 'Homepage Slide',
			'all_items' => 'All Sliders',
			'add_new' => 'Add New',
			'add_new_item' => 'Add New Slide',
			'edit' => 'Edit',
			'edit_item' => 'Edit Slide',
			'new_item' => 'New Slide',
			'view_item' => 'View Slide', 
			'search_items' => 'Search Homepage Slides',
			'not_found' =>  'Nothing found in the Database.',
			'not_found_in_trash' => 'Nothing found in Trash',
			'parent_item_colon' => ''
			),
			'description' => 'For use as the homepage slideshow.',
			'public' => false,
			'publicly_queryable' => false,
			'exclude_from_search' => false,
			'show_ui' => true,
			'show_in_nav_menus' => false,
			'query_var' => false,
			'menu_position' => 8,
			'menu_icon' => 'dashicons-admin-home',
			'rewrite'	=> false,
			'has_archive' => 'slider_type',
			'capability_type' => 'post',
			'hierarchical' => false,
			'register_meta_box_cb'	=> 'add_slider_metaboxes',
			'show_in_rest'		=> true,
			'supports' => array( 'title', 'page-attributes', 'editor', 'thumbnail' )
		)
	);

}

/**
 * Add a metabox to this custom post type
 *
 * @package WordPress
 * @subpackage evaero-boilerplate
 * @since Evaero Boilerplate 1.0
 *
 */
function add_slider_metaboxes() {
	add_meta_box('slider_page_link', 'Slide Link', 'add_slider_page_select', 'slider_type', 'side', 'default');
}

/**
 * Build the form input for use as the metabox
 *
 * @package WordPress
 * @subpackage evaero-boilerplate
 * @since Evaero Boilerplate 1.0
 *
 */
function add_slider_page_select(){
	global $post;

	//page link meta
	$page_link = get_post_meta($post->ID,'page_link', true);
	$page_cta = get_post_meta($post->ID,'page_cta', true);

	wp_nonce_field( 'slider_meta_box', 'slider_meta_box_nonce' );

	//pages group
	echo '<label>Pages/Posts</label><br>';

	echo '<select name="page_link" id="page_link">';
	echo '<option value="">Select Slide Destination</option>';

	echo '<optgroup label="Pages">';
	echo wp_list_pages(
			array(
				'title_li'	=> '',
				'depth'		=> 0,
				'echo'		=> 0,
				'walker'	=> new Select_Walker_Page($page_link)
			)
		);
	echo '</optgroup>';

	//posts group
	echo '<optgroup label="Posts">';
		$args = array( 'numberposts' => -1);
		$posts = get_posts($args);

	foreach( $posts as $post ) : setup_postdata($post);

		$selected = ($post->ID == $page_link) ? 'selected' : '';

		echo '<option value="' . $post->ID . '" ' . $selected . '>'. get_the_title() . '</option>';

	endforeach;

	echo '</optgroup></select>';

	echo '<br><br><label>Link Text</label><br>';
	echo '<input type="text" name="page_cta" value="'. $page_cta .'" >';
}




/**
 * Save the metabox data
 *
 * @package WordPress
 * @subpackage evaero-boilerplate
 * @since Evaero Boilerplate 1.0
 *
 */
function save_slider_meta($post_id, $post) {

	// verify this came from the our screen and with proper authorization,
	// because save_post can be triggered at other times
	if ( ! isset( $_POST['slider_meta_box_nonce'] ) ) {
		return;
	}

	// Verify that the nonce is valid.
	if ( ! wp_verify_nonce( $_POST['slider_meta_box_nonce'], 'slider_meta_box' ) ) {
		return;
	}


	// Is the user allowed to edit the post or page?
	if ( !current_user_can( 'edit_post', $post->ID ))
		return $post->ID;

		// OK, we're authenticated: we need to find and save the data
		// We'll put it into an array to make it easier to loop through.
		$slides_meta['page_link'] = $_POST['page_link'];
		$slides_meta['page_cta'] = $_POST['page_cta'];



		// Add values of $slides_meta as custom fields
		foreach ($slides_meta as $key => $value) { // Cycle through the $slides_meta array!
			if( $post->post_type == 'revision' ) return; // Don't store custom data twice
			$value = implode(',', (array)$value); // If $value is an array, make it a CSV (unlikely)

			if(get_post_meta($post->ID, $key, FALSE)) { // If the custom field already has a value
				update_post_meta($post->ID, $key, $value);
			} else { // If the custom field doesn't have a value
				add_post_meta($post->ID, $key, $value);
			}
			if(!$value) delete_post_meta($post->ID, $key); // Delete if blank
		}

}

// save the custom fields
add_action('save_post', 'save_slider_meta', 1, 2); 

// adding the function to the Wordpress init
add_action( 'init', 'slider_post_type');