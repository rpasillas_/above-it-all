<?php
/**
 * Custom Post type for Staff
 *
 * @package WordPress
 * @subpackage evaero-boilerplate
 * @since Evaero Boilerplate 1.0
 *
 */
function custom_post_staff() { 
	register_taxonomy( 'staff_cat', 
		array('staff'), 
		array('hierarchical' => true,
			'labels' => array(
				'name' => 'Staff Categories',
				'singular_name' => 'Staff Category',
				'search_items' =>  'Search Staff Categories',
				'all_items' => 'All Staff Categories',
				'parent_item' => 'Parent Staff Category',
				'parent_item_colon' => 'Parent Staff Category:',
				'edit_item' => 'Edit Staff Category',
				'update_item' => 'Update Staff Category',
				'add_new_item' => 'Add New Staff Category',
				'new_item_name' => 'New Staff Category Name',
			),
			'public'	=> true,
			'show_admin_column' => true, 
			'show_ui' => true,
			'query_var' => false,
			'rewrite' => array( 'slug' => 'the-experience/meet-our-staff', 'with_front'=> false ),
		)
	);
	
	// creating (registering) the custom type 
	register_post_type( 'staff', 
		// let's now add all the options for this post type
		array( 
			'labels' 	=> array(
					'name' => 'Staff', 			
					'singular_name' 	=> 'Staff  Member',
					'all_items' 		=> 'Staff', 
					'add_new' 		=> 'Add New', 
					'add_new_item' 	=> 'Add New Staff Member', 
					'edit' 			=> 'Edit', 
					'edit_item' 		=> 'Edit Staff', 
					'new_item' 		=> 'New Staff Member', 
					'view_item' 		=> 'View Staff Member', 
					'search_items' 		=> 'Search Staff', 
					'not_found' 		=> 'Nothing found in the Database.',  
					'not_found_in_trash' 	=> 'Nothing found in Trash', 
					'parent_item_colon' 	=> ''
				), 
			'public' 			=> true,
			'publicly_queryable' 	=> true,
			'exclude_from_search'=> false,
			'show_ui' 		=> true,
			'query_var' 		=> true,
			//'menu_position' 	=> 1,  
			'menu_icon' 		=> 'dashicons-businessman', 
			'rewrite'		=> array( 'slug' => 'the-experience/meet-our-staff', 'with_front' => false ), 
			'has_archive' 		=> 'the-experience/meet-our-staff', 
			'capability_type' 	=> 'post',
			'hierarchical' 		=> false,
			'show_in_rest'		=> true,			
			'supports' 		=> array( 'title', 'editor', 'thumbnail', 'page-attributes')
		) 
	); 
			
}

add_action( 'init', 'custom_post_staff');