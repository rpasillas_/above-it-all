<?php
/**
 * Custom Body Hook
 */
function _aia_do_hook(){
	do_action('aia_body'); 
}


function _aia_fb_sdk(){
?>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<?php
}

add_action('aia_body', '_aia_fb_sdk', 1);