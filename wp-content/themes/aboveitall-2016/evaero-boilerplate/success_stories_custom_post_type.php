<?php
/**
 * Custom Post type for Success Stories
 *
 * @package WordPress
 * @subpackage evaero-boilerplate
 * @since Evaero Boilerplate 1.0
 *
 */
function stories_post_type() { 
	register_taxonomy( 'stories_cat', 
		array('stories'), 
		array('hierarchical' => true,
			'labels' => array(
				'name' => 'Success Stories Categories',
				'singular_name' => 'Success Story Category',
				'search_items' =>  'Search Success Stories Categories',
				'all_items' => 'All Success Stories Categories',
				'parent_item' => 'Parent Success Stories Category',
				'parent_item_colon' => 'Parent Success Stories Category:',
				'edit_item' => 'Edit Success Stories Category',
				'update_item' => 'Update Success Stories Category',
				'add_new_item' => 'Add New Success Stories Category',
				'new_item_name' => 'New Success Stories Category Name',
			),
			'public'	=> true,
			'show_admin_column' => true, 
			'show_ui' => true,
			'query_var' => false,
		//	'rewrite' => array( 'slug' => 'community/alumni-addiction-recovery-success-stories', 'with_front'=> false ),
		)
	);
	
	// creating (registering) the custom type 
	register_post_type( 'stories', 
		// let's now add all the options for this post type
		array( 
			'labels' 	=> array(
					'name' => 'Success Stories', 			
					'singular_name' 	=> 'Success Story',
					'all_items' 		=> 'Success Stories', 
					'add_new' 			=> 'Add New', 
					'add_new_item' 		=> 'Add New Success Stories', 
					'edit' 				=> 'Edit', 
					'edit_item' 		=> 'Edit Success Stories', 
					'new_item' 			=> 'New Success Stories', 
					'view_item' 		=> 'View Success Stories', 
					'search_items' 		=> 'Search Success Stories', 
					'not_found' 		=> 'Nothing found in the Database.',  
					'not_found_in_trash' 	=> 'Nothing found in Trash', 
					'parent_item_colon' 	=> ''
				), 
			'public' 				=> true,
			'publicly_queryable' 	=> true,
			'exclude_from_search'=> false,
			'show_ui' 			=> true,
			'query_var' 		=> true,
			//'menu_position' 	=> 1,  
			'menu_icon' 		=> 'dashicons-format-quote', 
			'rewrite'			=> array( 'slug' => 'community/alumni-addiction-recovery-success-stories', 'with_front' => false ), 
			'has_archive' 		=> 'community/alumni-addiction-recovery-success-stories', 
			'capability_type' 	=> 'post',
			'hierarchical' 		=> false,
			'show_in_rest'		=> true,			
			'supports' 		=> array( 'title', 'editor', 'thumbnail', 'page-attributes')
		) 
	); 
			
}

add_action( 'init', 'stories_post_type');