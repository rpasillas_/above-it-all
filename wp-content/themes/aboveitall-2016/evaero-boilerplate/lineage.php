<?php
/**
 * Returns the ID of the top root ancestor of a page.
 *
 * @package WordPress
 * @subpackage evaero-boilerplate
 * @since Evaero Boilerplate 1.0
 *
 * @return integer|empty string of the post ancestor's ID or the post ID, or empty string as default.
 */
function get_top_parent_page_id() {
	global $post;
	$ancestors = $post->ancestors;
	if ($ancestors) {
		return end($ancestors);
	} else if( $post ) {
		return $post->ID;
	}else{
		return '';
	}
}

/**
 * Returns 'section' title.
 *
 * @package WordPress
 * @subpackage evaero-boilerplate
 * @since Evaero Boilerplate 1.0
 *
 * @return string value of the appropriate theme option of either the blog section, 
 *	     or a specific page section and defaults to the page title if no conditions met.
 */
function e_lineage_root_parent_title() {
	$id = get_top_parent_page_id();
	global $post;

	if ( is_archive( 'archive-staff' ) ) {
		return of_get_option( 'banner_title_85' );
	} else if (is_home() || is_single() || is_archive() || is_category()) {
		if( in_category(108) ){
			return 'Press Release';
		}else{
			return of_get_option( 'banner_title_blog' );
		}		
	} else if (is_404()) {
		return 'Page Not Found';
	} else if (is_page( 3477 )) {
		return of_get_option( 'banner_title_3477' );
	} else if ($id) {
		$title = of_get_option( 'banner_title_' . $id );

		if( $title ){
			return $title;	
		}else {
		 the_title($post->id);
		}
	} else {
		the_title($post->id);
	}
	
}

/**
 * Returns 'section' image.
 *
 * @package WordPress
 * @subpackage evaero-boilerplate
 * @since Evaero Boilerplate 1.0
 *
 * @return string value of the appropriate theme option of either the blog section, 
 *	     or a specific page section.
 */
function e_lineage_root_parent_img() {
	$id = get_top_parent_page_id();

	$image = '';

	if ( is_archive('staff')) {
		$image = of_get_option( 'banner_image_85' );

		if($image){
			$image = ' style="background-image:url(\'' . $image . '\');"';
		}
	} else if (is_home() || is_single() || is_archive()) {
		$image = of_get_option( 'banner_image_blog' );

		if($image){
			$image = ' style="background-image:url(\'' . $image . '\');"';
		}
	} else if (is_page( 3477 )) {
		$image = of_get_option( 'banner_image_3477' );

		if( $image ){
			$image = ' style="background-image:url(\'' . $image . '\');"';
		}else{
			$image = ' style="background-image:url(\'' . of_get_option( 'banner_image_default' ) . '\');"';
		}
	} else if ($id) {
		$image = of_get_option( 'banner_image_' . $id );

		if( $image ){
			$image = ' style="background-image:url(\'' . $image . '\');"';
		}else{
			$image = ' style="background-image:url(\'' . of_get_option( 'banner_image_default' ) . '\');"';
		}

	}


	return $image;
}
