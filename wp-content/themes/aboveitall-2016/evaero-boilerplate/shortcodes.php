<?php
/**
 * Evaero Shortcodes
 * Is dependent on theme options
 *
 * @package WordPress
 * @subpackage evaero-boilerplate
 * @since Evaero Boilerplate 1.0
 *
 */

	add_filter( 'the_content', 'do_shortcode');
	add_filter( 'post_content', 'do_shortcode');
	add_filter( 'the_excerpt', 'shortcode_unautop');
	add_filter( 'the_excerpt', 'do_shortcode');
	add_filter( 'widget_text', 'shortcode_unautop');
	add_filter( 'widget_text', 'do_shortcode');

//----- Panel
		function sc_panel($atts, $content = null) {
			extract( shortcode_atts(
				array(
					'title' => 'panel',
				),
				$atts ) );
			return '<div class="panel panel-default">
						<div class="panel-heading">' . esc_attr($title) . '</div>
						<div class="panel-body">
							<p>' . $content . '</p>
						</div>
					</div>';
		}
		add_shortcode('panel', 'sc_panel');

//----- Well
		function sc_well($atts, $content = null) {
			return '<div class="well">' . $content . '</div>';
		}
		add_shortcode('well', 'sc_well');

//----- Blockquote with Author
		function sc_blockquote($atts, $content = null) {
			extract(  shortcode_atts(
				array(
					'author' => 'blockquote',
				), $atts ) );

			return '<blockquote><p>' . $content . '</p><span class="author">' . esc_attr($author) . '</span></blockquote>';
		}
		add_shortcode('blockquote', 'sc_blockquote');

//----- Pullquote: Right
		function sc_pullquote($atts, $content = null) {
			return '<span class="pullquote right">' . $content . '</span>';
		}
		add_shortcode('pullquote', 'sc_pullquote');
		add_shortcode('pullquote_right', 'sc_pullquote');

//----- Pullquote: Left
		function sc_pullquote_left($atts, $content = null) {
			return '<span class="pullquote left">' . $content . '</span>';
		}
		add_shortcode('pullquote_left', 'sc_pullquote_left');

//----- Double-Column List
		function sc_twocol($atts, $content = null) {
			return '<div class="double clear">' . $content . '</div>';
		}
		add_shortcode('twocol', 'sc_twocol');

//----- Double-Column FLEX PARENT
		function sc_flexcol($atts, $content = null){
			return '<div class="flex-grid-double">' . $content . '</div>';
		}
		add_shortcode('flex-col', 'sc_flexcol');

//----- Double-Column FLEX ITEM
		function sc_flexitem($atts, $content = null){
			return '<div class="flex-grid-item">' . $content . '</div>';
		}
		add_shortcode('flex-item', 'sc_flexitem');

//----- Button
		function sc_button($atts, $content = null) {
			extract( shortcode_atts(
				array(
					'id' => 'button',
				),
				$atts ) );
			return '<a href="<?php echo get_page_link( \'' . esc_attr($id) . '\' ); ?>">' . $content . '</a>';
		}
		add_shortcode('button', 'sc_button');

//----- CTA: Giant
		function sc_cta_giant($atts, $content = null) {
			return '<span class="cta giant">' . $content . '<br>' . do_shortcode('[phone]') . '</span>';
		}
		add_shortcode('cta_giant', 'sc_cta_giant');

//----- CTA: Large
		function sc_cta_large($atts, $content = null) {
			return '<span class="cta large">' . $content . '<br>' . do_shortcode('[phone]') . '</span>';
		}
		add_shortcode('cta_large', 'sc_cta_large');

//----- CTA: Regular
		function sc_cta($atts, $content = null) {
			return '<span class="cta">' . $content  . '<br>' . do_shortcode('[phone]') . '</span>';
		}
		add_shortcode('cta', 'sc_cta');

//----- CTA: Small
		function sc_cta_small($atts, $content = null) {
			return '<span class="cta small">' . $content . '<br>' . do_shortcode('[phone]') . '</span>';
		}
		add_shortcode('cta_small', 'sc_cta_small');

//----- CTA: Mini
		function sc_cta_mini($atts, $content = null) {
			return '<span class="cta mini">' . $content . '<br>' . do_shortcode('[phone]') . '</span>';
		}
		add_shortcode('cta_mini', 'sc_cta_mini');

//----- CTA: Call Now
		function sc_cta_call_now($atts, $content = null) {
			return '<div class="call-now">' . $content . of_get_option( 'cta_bottom' ). '</div>';
		}
		add_shortcode('call-now', 'sc_cta_call_now');

//----- CTA: Bottom
		function sc_cta_bottom($atts, $content = null) {
			return '<div class="cta-bottom">' . of_get_option( 'cta_bottom' ). ' ' . do_shortcode('[phone]') . '</div>';
		}
		add_shortcode('bottom', 'sc_cta_bottom');
		add_shortcode('cta_bottom', 'sc_cta_bottom');

//----- Site URL
		function sc_site_url($atts, $content = null) {
			return 'http://';
		}
		add_shortcode('site-url', 'sc_site_url');

//----- ClearFIX
		function sc_clear($atts, $content = null) {
			return '<span class="clear">' . $content . '</span>';
		}
		add_shortcode('clear', 'sc_clear');

//----- Candybar
/**
 * Inserts a "candybar" template file into the content.
 * Any design element in the PSD comps that interrupt the page content
 * to display a call to action will need to put into its own template file
 * so that this shortcode can insert it anywhere in the content.
 * often times these "candybars" span the entire width of the browser
 * in these instances, the template file will need to begin with enough closing div tags
 * and end with enough opening div tags to allow the "candybar" to close the main content divs and allow
 * it span the entire browser width.
 */
		function candybar($atts){
			$data = '';
			ob_start();
			get_template_part('template-parts/part-candybar');
			$data = ob_get_contents();
			ob_end_clean();
			return $data;
		}
		add_shortcode('cta_banner', 'candybar');

//----- Address 1
		function sc_address_1($atts, $content = null) {
			$data = of_get_option( 'address' ) . '<br>';
			$data .= of_get_option( 'address_0a' ) . '<br>';
			$data .= of_get_option( 'city' ) . ', ';
			$data .= of_get_option( 'state' ) . ' ';
			$data .= of_get_option( 'zip' );
			return $data;
		}
		add_shortcode('address', 'sc_address_1');

//----- Address 2
		function sc_address_2($atts, $content = null) {
			$data = of_get_option( 'address_1' ) . '<br>';
			$data .= of_get_option( 'city_1' ) . ', ';
			$data .= of_get_option( 'state_1' ) . ' ';
			$data .= of_get_option( 'zip_1' );
			return $data;
		}
		add_shortcode('address_ups_fedex', 'sc_address_2');	

//----- Address 3
		function sc_address_3($atts, $content = null) {
			$data = of_get_option( 'address_2' ) . '<br>';
			$data .= of_get_option( 'city_2' ) . ', ';
			$data .= of_get_option( 'state_2' ) . ' ';
			$data .= of_get_option( 'zip_2' );
			return $data;
		}
		add_shortcode('address_usps', 'sc_address_3');		

//----- Email
		function sc_email($atts, $content = null) {
			return '<a href="mailto:' . of_get_option( 'email' ) . '">' . of_get_option( 'email' ) . '</a>';
		}
		add_shortcode('email', 'sc_email');

//----- Fax
		function sc_fax($atts, $content = null) {
			return $site_fax;
		}
		add_shortcode('fax', 'sc_fax');

//----- Phone
		function sc_phone($atts, $content = null) {
			$span  = of_get_option( 'call_tracking_span_class' );
			$num   = of_get_option( 'phone' );
			$rid   = array('-','(',')',' ');
			$clean = (str_replace($rid,'',$num));
			if ($span != '') {
				return '<span class="desktop phone ' . $span . ' clickable"><span class="num">' . $num . '</span></span>';
			} else if (strpos($num,'+') !== 'false') {
				return '<a class="desktop phone" href="tel:' . $clean . '"><span class="num">' . $num . '</span></a>';
			} else {
				return '<a class="desktop phone" href="tel:+' . $clean . '"><span class="num">' . $num . '</span></a>';
			}
		}
		add_shortcode('phone', 'sc_phone');

//----- Phone [shows phone icon]
		function sc_phone_alt($atts, $content = null) {
			$span  = of_get_option( 'call_tracking_span_class' );
			$num   = of_get_option( 'phone' );
			$rid   = array('-','(',')',' ');
			$clean = (str_replace($rid,'',$num));
			if ($span != '') {
				return '<svg class="phone-icon"><use xlink:href="#icon-phone"></use></svg><span class="desktop phone ' . $span . ' clickable"><span class="num">' . $num . '</span></span>';
			} else if (strpos($num,'+') !== 'false') {
				return '<svg class="phone-icon"><use xlink:href="#icon-phone"></use></svg><a class="desktop phone" href="tel:' . $clean . '"><span class="num">' . $num . '</span></a>';
			} else {
				return '<svg class="phone-icon"><use xlink:href="#icon-phone"></use></svg><a class="desktop phone" href="tel:+' . $clean . '"><span class="num">' . $num . '</span></a>';
			}
		}
		add_shortcode('phone_alt', 'sc_phone_alt');


//----- Current Clients Form
		function sc_current_clients_form($atts, $content = null){
			$data = '';
			ob_start();			
		?>
			<form id="pettyCash" action="https://www.merchante-solutions.com/jsp/tpg/secure_checkout.jsp" method="POST" name="pettycash">
				<input name="profile_id" type="hidden" value="49030004778100000002" />
				<input name="transaction_amount" type="hidden" value="" />
				<input name="css_url" type="hidden" value="" />
				<input name="cancel_url" type="hidden" value="" />
				<input name="return_url" type="hidden" value="" />
				<input name="logo_url" type="hidden" value="https://aboveitall.dev/wp-content/uploads/2014/06/aia-logo.png" />
				<input name="footer_url" type="hidden" value="" />
				<input name="merchant_email" type="hidden" value="info@aboveitalltreatment.com" />
				


				<h3>Billing Information</h3>
				<div class="form-half half-left">
					<label for="cardholder_first_name">First Name<span>*</span></label>
					<input maxlength="40" name="cardholder_first_name" size="30" type="text" />
				</div>
				
				<div class="form-half half-right">
					<label for="cardholder_last_name">Last Name<span>*</span></label>
					<input maxlength="40" name="cardholder_last_name" size="30" type="text" />
				</div>

				<div class="form-half half-left">
					<label for="cardholder_phone">Phone Number<span>*</span></label>
					<input maxlength="40" name="cardholder_phone" size="30" type="text" />
				</div>

				<div class="form-half half-right">
					<label for="customer_email">Email<span>*</span></label>
					<input maxlength="40" name="customer_email" size="30" type="text" />
				</div>
				
				<div class="form-half half-left">
					<label for="cardholder_street_address">Billing Address</label>
					<input maxlength="40" name="cardholder_street_address" size="30" type="text" />
				</div>
				
				<div class="form-half half-right">
					<label for="cardholder_zip">Billing Zip</label>
					<input maxlength="9" name="cardholder_zip" size="10" type="text" />
				</div>

				
				<h3>Patient Information (Sending money to)</h3>	
				<div class="form-half half-left">
					<label for="ship_to_first_name">First Name<span>*</span></label>
					<input maxlength="40" name="ship_to_first_name" size="30" type="text" />
				</div>
				
				<div class="form-half half-right">
					<label for="ship_to_last_name">Last Name<span>*</span></label>
					<input maxlength="40" name="ship_to_last_name" size="30" type="text" />
				</div>

				<div class="form-half half-left">
					<label for="prefee">Amount $<span>*</span></label>
					<input id="prefee" name="prefee" type="tel" />
				</div>
				
				<div class="form-half half-right">
					<label for="amount">Total Charge $ (+5% Convenience Fee)</label>
					<input id="amount" name="amount" readonly="readonly" type="number" />
				</div>

				<div class="form-half half-right submit-wrap">
					<label for="subbut">* Required Fields</label>
					<input class="formFields" name="subbut" type="submit" value="Continue" />
				</div>		

			</form>	
		<?php
			$data = ob_get_contents();
			ob_end_clean();
			return $data;
		}
		add_shortcode( 'cc_form', 'sc_current_clients_form');


//----- Flex Grid
		function sc_fg($atts, $content = null) {
			extract( shortcode_atts(
				array(
					'padding' => '',
				),
				$atts ) );

			if($padding){
				$style = ' style="padding: 0 ' . $padding . 'px;"';
			}

			return '<div class="flex-grid"' . $style . '>' . $content . '</div>';
		}
		add_shortcode( 'flex-grid', 'sc_fg');


//----- Flex Grid Item
		function sc_fgi($atts, $content = null) {
			extract( shortcode_atts(
				array(
					'columns' => '',
				),
				$atts ) );

			return '<div class="flex-grid-item flex-grid-item-"' . $columns . '>' . $content . '</div>';
		}
		add_shortcode( 'flex-grid-item', 'sc_fgi');		


//----- Drug Icon Flex Grid Item
		function sc_fgi_icon($atts, $content = null) {
			extract( shortcode_atts(
				array(
					'columns' => '2',
					'icon'	=> '',
				),
				$atts ) );

			return '<div class="flex-grid-item flex-grid-item-icon flex-grid-item-' . $columns . '"><div class="icon-wrap"><img src="' . $icon . '" alt="' . $content . '"></div><span>' . $content . '</span></div>';
		}
		add_shortcode( 'flex-grid-item-icon', 'sc_fgi_icon');				


//----- Address Box Flex Grid Item
		function sc_fci_address($atts, $content = null) {
			extract( shortcode_atts(
				array(
					'title' => '',
				),
				$atts ) );

			$address = ( $title=='USPS')? do_shortcode('[address_usps]') : do_shortcode('[address_ups_fedex]');
			
			$data = '<div class="grid-item">';
			$data .= '<h3>' . $title . '</h3>';
			$data .= '<div>';
			$data .= '<p>' . $content . '</p>';
			$data .= get_bloginfo( 'name' ) . '<br>';
			$data .= 'Attn: Client\'s Name<br>';
			$data .= $address;
			$data .= '</div>';
			$data .= '</div>';

			return $data;
		}
		add_shortcode( 'flex-grid-item-address', 'sc_fci_address');