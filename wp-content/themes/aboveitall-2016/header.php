<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Above it All
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<?php global $is_IE; if ( $is_IE ) : ?>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<?php endif; ?>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php _aia_do_hook(); ?>
<span class="svg-defs"><?php aia_include_svg_icons(); ?></span>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#main"><?php esc_html_e( 'Skip to content', 'aia' ); ?></a>

	<header id="masthead"  class="site-header">		

		<div class="site-branding">
			<div class="wrap">
				<div class="site-title">
					<div class="site-title-inner">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><svg class="logo"><use xlink:href="#icon-aia-logo"></use></svg></a>
						<svg class="ribbon"><use xlink:href="#icon-ribbon"></use></svg>
					</div>
				</div>

				<div class="header-phone-links">
					<div class="headphone">1 (866) 954-3420</div>
					<a href="http://www.prweb.com/releases/2015/05/prweb12695111.htm" class="ae-logo" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/a-and-e.png" alt="as seen on a&amp;e"></a>
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/carf-sm.png" alt="CARF Accredited">
					<div class="phone-wrap">
						<svg class="phone-icon"><use xlink:href="#icon-phone"></use></svg>
						<?php echo do_shortcode('[phone]'); ?>
						<span class="conf-msg">Confidential Admissions Line</span>
					</div>
					<?php
						wp_nav_menu(array(
							'theme_location' => 'secondary',
							'menu_class'	=> 'secondary-nav',
							'container'	=> null
						));
					?>
				</div>

			</div><!-- .wrap -->		
		</div><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation">
			<div class="wrap">				
				<?php
					wp_nav_menu( array(
						'theme_location' => 'primary',
						'menu_id'        => 'primary-menu',
						'menu_class'     => 'menu dropdown',
						'walker'	=> new nav_walker_menu
					) ); 
				?>
				<?php
					wp_nav_menu( array(
						'theme_location' => 'primary-mobile',
						'menu_id'        => 'primary-menu-mobile',
						'menu_class'     => 'menu dropdown',
						'walker'	=> new nav_walker_menu
					) ); 
				?>
			</div><!-- .wrap -->
		</nav><!-- #site-navigation -->
		
	</header><!-- #masthead -->

	<?php echo aia_do_social_icons( 'fixed' ); ?>

	<div id="content" class="site-content">

	<?php 
		if( is_front_page() ){
			get_template_part('template-parts/banner', 'home');
		}else{
			get_template_part('template-parts/banner', 'interior');
		}
	?>