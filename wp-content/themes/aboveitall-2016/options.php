<?php
/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 */
function optionsframework_option_name() {
	// Change this to use your theme slug
	return 'options-framework-theme';
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 *
 * If you are making your theme translatable, you should replace 'theme-textdomain'
 * with the actual text domain for your theme.  Read more:
 * http://codex.wordpress.org/Function_Reference/load_theme_textdomain
 */

function optionsframework_options() {

	// Pull all the pages into an array
	$options_pages = array();
	$options_pages_obj = get_pages( 'sort_column=post_parent,menu_order' );
	$options_pages[''] = 'Select a page:';
	foreach ($options_pages_obj as $page) {
		$options_pages[$page->ID] = $page->post_title;
	}




	$options = array();


	/* =========================*\
	 *	CONTACT INFO
	\* =========================*/
	$options[] = array(
		'name' => 'Contact Info',
		'type' => 'heading'
	);

	$options[] = array(
		'name' => 'Phone',
		'desc' => '',
		'id' => 'phone',
		'class' => 'mini',
		'type' => 'text'
	);

	$options[] = array(
		'name' => 'Phone: Non-Admissions',
		'desc' => '',
		'id' => 'phone_alt',
		'class' => 'mini',
		'type' => 'text'
	);

	// $options[] = array(
	// 	'name' => 'Fax',
	// 	'desc' => '',
	// 	'id' => 'fax',
	// 	'class' => 'mini',
	// 	'type' => 'text'
	// );

	// $options[] = array(
	// 	'name' => 'Email',
	// 	'desc' => '',
	// 	'id' => 'email',
	// 	'class' => 'mini',
	// 	'type' => 'text'
	// );


	
	/* =========================*\
	 *	Address 1
	\* =========================*/
	$options[] = array(
		'name' => 'Address (Main)',
		'type' => 'heading'
	);

	$options[] = array(
		'name' => 'Street Address',
		'desc' => '',
		'id' => 'address',
		'type' => 'text'
	);

	$options[] = array(
		'name' => 'Street Address Line 2',
		'desc' => '',
		'id' => 'address_0a',
		'type' => 'text'
	);

	$options[] = array(
		'name' => 'City',
		'desc' => '',
		'id' => 'city',
		'type' => 'text'
	);

	$options[] = array(
		'name' => 'State',
		'desc' => '',
		'id' => 'state',
		'class' => 'mini',
		'type' => 'text'
	);

	$options[] = array(
		'name' => 'Zip Code',
		'desc' => '',
		'id' => 'zip',
		'class' => 'mini',
		'type' => 'text'
	);


	/* =========================*\
	 *	Address 2
	\* =========================*/
	$options[] = array(
		'name' => 'Address (UPS & FedEx)',
		'type' => 'heading'
	);

	$options[] = array(
		'name' => 'Street Address',
		'desc' => '',
		'id' => 'address_1',
		'type' => 'text'
	);

	$options[] = array(
		'name' => 'City',
		'desc' => '',
		'id' => 'city_1',
		'type' => 'text'
	);

	$options[] = array(
		'name' => 'State',
		'desc' => '',
		'id' => 'state_1',
		'class' => 'mini',
		'type' => 'text'
	);

	$options[] = array(
		'name' => 'Zip Code',
		'desc' => '',
		'id' => 'zip_1',
		'class' => 'mini',
		'type' => 'text'
	);


	/* =========================*\
	 *	Address 3
	\* =========================*/
	$options[] = array(
		'name' => 'Address (USPS)',
		'type' => 'heading'
	);

	$options[] = array(
		'name' => 'Street Address',
		'desc' => '',
		'id' => 'address_2',
		'type' => 'text'
	);

	$options[] = array(
		'name' => 'City',
		'desc' => '',
		'id' => 'city_2',
		'type' => 'text'
	);

	$options[] = array(
		'name' => 'State',
		'desc' => '',
		'id' => 'state_2',
		'class' => 'mini',
		'type' => 'text'
	);

	$options[] = array(
		'name' => 'Zip Code',
		'desc' => '',
		'id' => 'zip_2',
		'class' => 'mini',
		'type' => 'text'
	);


	
	/* =========================*\
	 *	PPC
	\* =========================*/

	$options[] = array(
		'name' => 'PPC Settings',
		'type' => 'heading'
	);

			$options[] = array(
				'name' => 'Google Analytics',
				'desc' => '',
				'id' => 'ga_analytics',
				'type' => 'textarea'
			);

			$options[] = array(
				'name' => 'Google Remarketing',
				'desc' => '',
				'id' => 'g_remarketing',
				'type' => 'textarea'
			);

			$options[] = array(
				'name' => 'Call Tracking',
				'desc' => '',
				'id' => 'call_tracking',
				'type' => 'textarea'
			);

			$options[] = array(
				'name' => 'Call Tracking Span Class',
				'desc' => '',
				'id' => 'call_tracking_span_class',
				'type' => 'text',
				'class' => 'mini'
			);

			$options[] = array(
				'name' => 'SnapEngage',
				'desc' => '',
				'id' => 'snap_engage',
				'type' => 'textarea'
			);

			$options[] = array(
				'name' => 'Miscellaneous Scripts',
				'desc' => '',
				'id' => 'misc_scripts',
				'type' => 'textarea'
			);



	/* =========================*\
	 *	CONTENT
	\* =========================*/
	// $options[] = array(
	// 	'name' => 'Content Settings',
	// 	'type' => 'heading'
	// );
	// 	$options[] = array(
	// 		'name' => 'CTA Bottom',
	// 		'id' => 'cta_bottom',
	// 		'type' => 'textarea'
	// 	);
	// 	$options[] = array(
	// 		'name' => 'Blog Page Introduction',
	// 		'id' => 'blog_intro',
	// 		'type' => 'textarea'
	// 	);
		



	/* =========================*\
	 *	SECTION BANNERS
	\* =========================*/

	$options[] = array(
		'name' => 'Section Banners',
		'type' => 'heading'
	);
		/**
		 * Each main navigation item will need it's own set of options
		 * A set of options consists of a image and a text field
		 *
		 * The image serves as the header banner background image
		 * The text field serves as the banner title
		 *
		 * Each set of options has an ID which ends in the ID number
		 * that corresponds to that page.
		 * 'banner_title_{ID}' and 'banner_image_{ID}' 
		 *
		 * For example, given the following site navigation
		 * Services | Blog | Contact
		 * Services page has ID of 1, Contact page has ID of 3, Blog page, is different and will address later.
		 * The theme options for Services will use 'id' => 'banner_title_1' for the array ID as demonstrated below.
		 *
		 * Since blog page is a WP Reading page, this theme option ID can be generic, like 'banner_title_blog'
		 *
		 * These ID values are used by inc/lineage.php file
		 */

		//---- TREATMENT PROGRAMS
		$options[] = array(
			'name'	=> 'Banner Title: Treatment Programs',
			'desc'	=> '',
			'id' 	=> 'banner_title_51',
			'std'	=> 'Treatment Programs',
			'type' 	=> 'text'
		);
		$options[] = array(
			'name' 	=> 'Banner Image: Treatment Programs',
			'desc' 	=> '',
			'id' 	=> 'banner_image_51',
			'type' 	=> 'upload'
		);


		//---- DRUG REHAB
		$options[] = array(
			'name'	=> 'Banner Title: Drug Rehab',
			'desc'	=> '',
			'id' 	=> 'banner_title_49',
			'std'	=> 'Drug Rehab',
			'type' 	=> 'text'
		);
		$options[] = array(
			'name' 	=> 'Banner Image: Drug Rehab',
			'desc' 	=> '',
			'id' 	=> 'banner_image_49',
			'type' 	=> 'upload'
		);


		//---- REHABILITATION PROCESS
		$options[] = array(
			'name'	=> 'Banner Title: Rehabilitation Process',
			'desc'	=> '',
			'id' 	=> 'banner_title_52',
			'std'	=> 'Rehabilitation Process',
			'type' 	=> 'text'
		);
		$options[] = array(
			'name' 	=> 'Banner Image: Rehabilitation Process',
			'desc' 	=> '',
			'id' 	=> 'banner_image_52',
			'type' 	=> 'upload'
		);


		//---- EXPERIENCE ABOVE IT ALL
		$options[] = array(
			'name'	=> 'Banner Title: Experience Above It All',
			'desc'	=> '',
			'id' 	=> 'banner_title_85',
			'std'	=> 'Experience Above It All',
			'type' 	=> 'text'
		);
		$options[] = array(
			'name' 	=> 'Banner Image: Experience Above It All',
			'desc' 	=> '',
			'id' 	=> 'banner_image_85',
			'type' 	=> 'upload'
		);


		//---- COMMUNITY
		$options[] = array(
			'name'	=> 'Banner Title: Community',
			'desc'	=> '',
			'id' 	=> 'banner_title_4456',
			'std'	=> 'Community',
			'type' 	=> 'text'
		);
		$options[] = array(
			'name' 	=> 'Banner Image: Community',
			'desc' 	=> '',
			'id' 	=> 'banner_image_4456',
			'type' 	=> 'upload'
		);


		//---- BLOG
		$options[] = array(
			'name' 	=> 'Banner Title: Blog',
			'desc' 	=> '',
			'id' 	=> 'banner_title_blog',
			'std'	=> 'Blog',
			'type' 	=> 'text'
		);
		$options[] = array(
			'name' 	=> 'Banner Image: Blog',
			'desc' 	=> '',
			'id' 	=> 'banner_image_blog',
			'type' 	=> 'upload'
		);


		//---- CONTACT
		//---- ASSUMES A CONTACT PAGE WITH PAGE ID 3
		$options[] = array(
			'name' 	=> 'Banner Title: Contact',
			'desc' 	=> '',
			'id' 	=> 'banner_title_41',
			'std'	=> 'Contact',
			'type' 	=> 'text'
		);
		$options[] = array(
			'name' 	=> 'Banner Image: Contact',
			'desc' 	=> '',
			'id' 	=> 'banner_image_41',
			'type' 	=> 'upload'
		);


		//---- CURRENT CLIENTS
		$options[] = array(
			'name' 	=> 'Banner Title: Current Clients',
			'desc' 	=> '',
			'id' 	=> 'banner_title_3477',
			'std'	=> 'Current Clients',
			'type' 	=> 'text'
		);
		$options[] = array(
			'name' 	=> 'Banner Image: Current Clients',
			'desc' 	=> '',
			'id' 	=> 'banner_image_3477',
			'type' 	=> 'upload'
		);


		//---- DEFAULT
		$options[] = array(
			'name' 	=> 'Banner Image: Default',
			'desc' 	=> '',
			'id' 	=> 'banner_image_default',
			'type' 	=> 'upload'
		);		

	/* =========================*\
	 *	ROTATING TESTIMONIALS
	\* =========================*/
	$options[] = array(
		'name' => 'Rotating Testimonials',
		'type' => 'heading'
	);

		$options[] = array(
			'name' => 'Testimonial 1',
			'desc' => '',
			'id' => 'testimonial_1',
			'type' => 'text'
		);
		$options[] = array(
			'name' => 'Testimonial 2',
			'desc' => '',
			'id' => 'testimonial_2',
			'type' => 'text'
		);
		$options[] = array(
			'name' => 'Testimonial 3',
			'desc' => '',
			'id' => 'testimonial_3',
			'type' => 'text'
		);






	return $options;
}
