<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Above it All
 */

get_header(); ?>
	
	<div class="home-form">
		<div class="wrap">
			<?php echo do_shortcode( '[contact-form-7 id="4654" title="Home"]' ); ?>
		</div>
	</div>

	<div class="funnels">
		<div class="wrap">
			<span class="h1">Seeking Addiction Treatment?</span>
			<div class="funnel funnel-loved-ones">
				<img src="<?php echo get_theme_mod('aia_funnel_loved_one_thumb'); ?>" >
				<a href="<?php echo get_permalink(4501); ?>" class="h3">For Loved Ones</a>
				<?php
					wp_nav_menu( array(
						'theme_location' => 'funnel-loved-ones',
						'container'	=> false
					) ); 
				?>
			</div>
			<div class="funnel funnel-myself">
				<img src="<?php echo get_theme_mod('aia_funnel_myself_thumb'); ?>" >
				<a href="<?php echo get_permalink(4503); ?>" class="h3">For Myself</a>
				<?php
					wp_nav_menu( array(
						'theme_location' => 'funnel-myself',
						'container'	=> false
					) ); 
				?>
			</div>
		</div>
	</div>

	<?php get_template_part('template-parts/tour', 'gallery'); ?>

	<div class="wrap">
		<div class="primary content-area">
			

			<main id="main" class="site-main" role="main">

				<article <?php post_class(); ?> data-template="content-page">
					<header class="entry-header">
						<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
					</header><!-- .entry-header -->
					<div class="logo-separator">
						<svg><use xlink:href="#icon-aia-logo-mountain"></use></svg>
					</div>

					<div class="entry-content">
						<?php
							the_content();

							wp_link_pages( array(
								'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'aia' ),
								'after'  => '</div>',
							) );
						?>
					</div><!-- .entry-content -->
				</article><!-- #post-## -->

				
				<div class="home-cta">
					<span class="h4">Speak with a Treatment Specialist to Learn More.</span>

					<span class="call-msg">Call Now</span>
					<div class="phone-box">
						<?php echo do_shortcode( '[phone]' ); ?>
						<span>Toll-Free &amp; Confidential</span>
					</div>
				</div>

			</main><!-- #main -->
		</div><!-- .primary -->

	</div><!-- .wrap -->

	<?php get_template_part('template-parts/insurance');  ?>

	<div class="wrap">
		<div class="primary content-raea">
			<div class="site-main">
				<h2><?php the_field( 'auxiliary_title' ); ?></h2>
				<div class="logo-separator">
					<svg><use xlink:href="#icon-aia-logo-mountain"></use></svg>
				</div>
				<?php the_field( 'auxiliary_content' ); ?>
			</div>
		</div>
	</div>

<?php get_template_part('template-parts/contact', 'form');  ?>
<?php get_template_part('template-parts/intervention'); ?>
<?php get_template_part('template-parts/success', 'stories');  ?>
<?php get_footer(); ?>