<?php
/*
** Template Name: Landing Page #5
*/
?>
<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#" lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=688">
		<link rel="shortcut icon" href="favicon.ico">

		<title><?php wp_title(); ?></title>

		<meta prefix="og: http://ogp.me/ns#" property="og:title" content="<?php echo get_the_title(); ?>"/>
		<meta prefix="og: http://ogp.me/ns#" property="og:image" content="<?php thumb_url(); ?>"/>
		<meta name="google-translate-customization" content="7b002b4f23af8687-57c38087baaaf3b3-g707f7eae41e165c3-17"></meta>
		<link rel="image_src" href="<?php thumb_url(); ?>" />

		<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/style.css" rel="stylesheet">
		<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/swiper.css" rel="stylesheet">

		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
		<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
		<script async src="//5239.tctm.co/t.js"></script>

		<?php if ( ot_get_option( 'ppc_google_analytics' ) ) echo '<script type="text/javascript">' . "\n" . ot_get_option( 'ppc_google_analytics' ) . "\n\t\t" . '</script>'; ?>
		<?php if ( ot_get_option( 'ppc_google_remarketing' ) ) echo ot_get_option( 'ppc_google_remarketing' ); ?>
		<?php if ( ot_get_option( 'ppc_infinity' ) ) echo ot_get_option( 'ppc_infinity' ); ?>

		<?php wp_head(); ?>
	</head>

	<body class="home">
        
		<nav>
			<div class="navbar navbar-default navbar-fixed-top" role="navigation">
				<div class="container">
					<div class="navbar-header">
						<a class="navbar-brand" href="/">Home</a>
						<span class="phones">
							<span><?php echo do_shortcode('[phone]'); ?></span>
							<span>Non-Admissions Call: <?php echo do_shortcode('[phone_noadmit]'); ?></span>
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="sr-only">Toggle navigation</span>
						</button>
						</span>
					</div>
					<div class="collapse navbar-collapse">
						<?php wp_nav_menu( array('menu' => 'top', 'menu_class' => 'nav navbar-nav', 'container' => '' )); ?>
					</div>
				</div>
			</div>
		</nav>

	<header>
		<div class="bg"></div>
		<div class="container">
			<div class="col-md-12 intro">
				<div class="table-outer">
				<div class="table-inner">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo-header-home.png" />
				<span class="section">Total Recovery from Drug &amp; Alcohol Addiction</span>
				<span class="button desktop"><a href="/contact">Get Help Today</a></span>
				<span class="button mobile"><a href="tel:<?php echo ot_get_option('phone'); ?>">Call Now</a></span>
				</div></div>
			</div>
		</div>
	</header>

	<div class="welcome">
		<div class="container">
			<div class="col-md-12">
				<span class="title"><span><?php echo get_field('intro_title'); ?></span></span>
				<p><?php echo get_field('intro_content'); ?></p>
			</div>
		</div>
	</div>

	<div class="gallery">
		<div class="row-fluid">
			<div class="col-md-6"></div>
			<div class="col-md-3"></div>
			<div class="col-md-3"></div>
			<div class="col-md-3"></div>
			<div class="col-md-3">
				<div class="table-outer">
					<div class="table-inner">
						<span><a href="<?php echo get_page_link(85); ?>">View More Photos</a></span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="content">
		<div class="container">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<div class="col-md-12">
					<h1><span><?php the_title(); ?></span></h1>
					<?php get_template_part( 'thumbnail' ); ?>
					<?php the_content(); ?>
				</div>
			<?php endwhile; endif; ?>
		</div>
	</div>

	<div class="client-test">
		<div class="container">
			<div class="col-md-6">
				<span class="title">Client Testimonials</span>
				<p>Watch videos from real Above It All clients sharing their experiences or read testimonials from clients and their families.</p>
				<div class="quote">
					<span class="text"><?php echo ot_get_option( 'featured_testimonial_quote' ); ?></span>
					<span class="author"><?php echo ot_get_option( 'featured_testimonial_author' ); ?></span>
				</div>
				<span class="more"><a href="<?php echo get_page_link(47); ?>">View More Testimonials</a></span>
			</div>
			<div class="col-md-6">
				<iframe width="560" height="315" src="//www.youtube.com/embed/_mieen64dSA?rel=0&autoplay=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
			</div>
		</div>
	</div>

	<div class="insurance">
		<div class="container">
			<div class="col-md-6">
				<span class="title">Most Insurance Accepted</span>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/insurance-unitedhealthcare.png" />
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/insurance-bluecrossblueshield.png" />
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/insurance-aetna.png" />
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/insurance-cigna.png" />
			</div>
			<div class="col-md-6">
				<span class="title">Standards &amp; Best Practices</span>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/standards-carf.png" />
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/standards-naatp.png" />
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/standards-cdhcs.png" />
			</div>
		</div>
	</div>

<div class="content">
	<div class="container" itemscope itemtype="http://schema.org/Article">

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<div class="col-md-8">
			<h1 itemprop="name"><?php echo the_field('3rdblock_title'); ?></h1>
		</div>
		<div class="col-md-4"></div>
		<?php if (is_page('contact')) {
			echo '<div class="col-md-12 body-text" itemscope itemtype="http://schema.org/Article">';
		} else {
			echo '<div class="col-md-8 body-text" itemscope itemtype="http://schema.org/Article">';
		} ?>
			<?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
			<?php echo the_field('3rdblock_text'); ?>
		</div>
<?php endwhile; endif; ?>

<?php get_sidebar(); ?>
				</div>
			</div>
		</div>
	<footer>
			<div class="cats">
				<div class="container">
					<div class="col-md-12">
						<span class="title">Blog Categories</span>
						<span class="subtitle">Select a category below to find a post on that particular topic.</span>
						<ul>
							<?php wp_list_categories('orderby=name&title_li='); ?>
						</ul>
					</div>
				</div>
			</div>
			<div class="mountains">
				<div class="container">
					<div class="col-md-12">
						<span class="title"><?php echo ot_get_option( 'footer_mountains_title' ); ?></span>
						<p class="content-top"><?php echo ot_get_option( 'footer_mountains_content_top' ); ?></p>
					</div>
					<div class="col-md-6">
						<p class="content-bottom"><?php echo ot_get_option( 'footer_mountains_content_bottom' ); ?></p>
						<a class="get-help" href="<?php echo get_permalink(get_page_by_path('contact')); ?>">Get Help Today</a>
					</div>
					<div class="col-md-6">
					</div>
				</div>
			</div>
			<div class="bottom">
				<div class="container">
					<div class="col-md-3 menu">
						<?php wp_nav_menu( array( 'theme_location' => 'footer-main', 'menu_class' => 'main', 'container' => '', 'items_wrap' => '<ul>%3$s</ul>' ) ); ?>
					</div>
					<div class="col-md-3 social">
						<p><span>Get Social With AIA</span>
						<ul>
							<li><a href="https://www.facebook.com/<?php echo ot_get_option( 'social_facebook' ); ?>" target="_blank" class="facebook">Facebook</a></li>
							<li><a href="http://www.twitter.com/<?php echo ot_get_option( 'social_twitter' ); ?>" target="_blank" class="twitter">Twitter</a></li>
							<li><a href="https://plus.google.com/<?php echo ot_get_option( 'social_googleplus' ); ?>" target="_blank" class="gplus">Google+</a></li>
							<li><a href="https://www.pinterest.com/<?php echo ot_get_option( 'social_pinterest' ); ?>" target="_blank" class="pinterest">Pinterest</a></li>
							<li><a href="http://www.youtube.com/user/<?php echo ot_get_option( 'social_youtube' ); ?>" target="_blank" class="youtube">YouTube</a></li>
							<li><a href="https://linkedin.com/company/<?php echo ot_get_option( 'social_linkedin' ); ?>" target="_blank" class="linkedin">LinkedIn</a></li>
						</ul>
					</div>
					<div class="col-md-4 hotline">
						<span class="call-today">
							<span>Help is waiting.</span>
							<span>Call us today!</span>
							<span><?php echo do_shortcode('[phone]'); ?></span>
							<span>24/7 Confidential Hotline</span>
						</span>
					</div>
					<div class="col-md-2 ahcl">
						<a href="https://app.americanhealthcarelending.com/003738" target="_blank"><img src="http://www.americanhealthcarelending.com/a/public/images/White/images/White-Buttons_15.jpg" alt="American Healthcare Lending" /></a>
					</div>
					<div class="col-md-12 sublinks">
						<?php wp_nav_menu( array( 'theme_location' => 'footer-sub', 'menu_class' => 'sub', 'container' => '', 'items_wrap' => '<ul>%3$s</ul>' ) ); ?>
					</div>
					<div class="col-md-12 copyright">
						<p>Copyright &copy; <?php copyright(); ?> by Above It All Treatment Center. All Rights Reserved.</p>
				</div>
			</div>
		</footer>

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/bootstrap.min.js"></script>
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/general.js"></script>
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/fancybox.js"/></script>
		<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/fancybox.css" type="text/css" media="screen" />
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/mousewheel-3.0.6.pack.js"/></script>
		<script type="text/javascript">
			$(document).ready(function() {
				$('.fancybox').fancybox();
		</script>
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/swiper.2.1.min.js"></script>
		<script>
			var mySwiper = new Swiper('.swiper-container',{
				pagination: '.pagination',
				loop:true,
				grabCursor: true,
				paginationClickable: true
			})
			$('.arrow-left').on('click', function(e){
				e.preventDefault()
				mySwiper.swipePrev()
			})
			$('.arrow-right').on('click', function(e){
				e.preventDefault()
				mySwiper.swipeNext()
			})
		</script>

		<?php wp_footer(); ?>
	</body>
</html>