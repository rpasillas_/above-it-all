<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Above it All
 */

?>

	</div><!-- #content -->

	<div class="footer-area">
		<?php wds_page_builder_area( 'after_content' ); ?>
	</div>

	<?php						
		if( is_page(41) ){
			get_template_part('template-parts/part', 'insurance');
			get_template_part('template-parts/part', 'ae');
			get_template_part('template-parts/part', 'posts');
		}elseif( is_front_page() ){
			get_template_part('template-parts/part', 'contact');
			get_template_part('template-parts/part', 'ae');
			get_template_part('template-parts/part', 'posts');
			get_template_part('template-parts/part', 'home-additional');
		}
		else{
			get_template_part('template-parts/part', 'insurance');
			get_template_part('template-parts/part', 'posts');
			get_template_part('template-parts/part', 'contact');
		}
	?>


	<footer id="colophon" class="site-footer">
		<div class="wrap footer-top">
			
			<div class="footer-col footer-logo">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/aia-logo-alt.svg" alt="<?php echo bloginfo('name');?>">
			</div>

			<div class="footer-col footer-cta">
				<div class="footer-cta-inner">
					<span class="line-1"><?php echo get_theme_mod('_aia_cta_text_1'); ?></span>
					<span class="line-2"><?php echo get_theme_mod('_aia_cta_text_2'); ?></span>
					<?php echo do_shortcode('[phone]'); ?>
					<span class="line-3"><?php echo get_theme_mod('_aia_cta_phone_text'); ?></span>
				</div>
			</div>

			<div class="footer-col footer-social">
				<span class="h6">Let's Get Social!</span>
				<?php aia_do_social_icons(); ?>
			</div>

			<div class="footer-col footer-loan">
				<a href="https://app.americanhealthcarelending.com/003738" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/prosper.png" alt="Prosper Healthcare Lending" /></a>
			</div>		

		</div><!-- .wrap -->

		<div class="wrap footer-bottom">

			<div class="site-info">
				<?php
					wp_nav_menu(array(
						'theme_location' => 'footer',
						'menu_class'	=> 'menu',
						'container'	=> null
					));
				?>

				<?php _aia_do_copyright_text(); ?>
			</div><!-- .site-info -->

		</div><!-- .wrap -->



	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
