<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Above it All
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<?php global $is_IE; if ( $is_IE ) : ?>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<?php endif; ?>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php _aia_include_svg_definitions(); ?>
<?php _aia_do_hook(); ?>

<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#main"><?php esc_html_e( 'Skip to content', 'aia' ); ?></a>

	<?php wds_page_builder_area( 'hero' ); ?>

	<header id="masthead" class="site-header">
		
		<div class="site-branding">
			<div class="wrap">
				<div class="site-title">
					<div class="site-title-inner">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><svg class="logo"><use xlink:href="#icon-aia-logo"></use></svg></a>
						<svg class="ribbon"><use xlink:href="#icon-ribbon"></use></svg>
					</div>
				</div>


				<div class="header-phone-links">
					<svg class="ae-logo"><use xlink:href="#icon-a_and_e_logo"></use></svg>
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/carf-sm.png" alt="CARF Accredited">
					<svg class="phone-icon"><use xlink:href="#icon-phone"></use></svg>
					<?php echo do_shortcode('[phone]'); ?>
					<?php
						wp_nav_menu(array(
							'theme_location' => 'sub-menu',
							'menu_class'	=> 'sub-menu',
							'container'	=> null
						));
					?>
				</div>

			</div><!-- .wrap -->
		</div><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation">
			<div class="wrap">
				<?php
					wp_nav_menu( array(
						'theme_location' => 'primary',
						'menu_id'        => 'primary-menu',
						'menu_class'     => 'menu dropdown'
					) );
				?>
			</div><!-- .wrap -->
		</nav><!-- #site-navigation -->

		
	</header><!-- #masthead -->

	<?php wds_page_builder_area( 'before_content' ); ?>

	<?php
		if( !is_front_page() ){
			get_template_part('template-parts/part', 'banner');
		}
	?>
	
	<div id="content" class="site-content">
