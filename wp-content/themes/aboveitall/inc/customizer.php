<?php
/**
 * Above it All Theme Customizer.
 *
 * @package Above it All
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function _aia_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	// Add our social link options
    $wp_customize->add_section(
        '_aia_social_links_section',
        array(
            'title'       => __( 'Social Links', 'aia' ),
            'description' => __( 'These are the settings for social links. Please limit the number of social links to 6.', 'aia' ),
            'priority'    => 90,
        )
    );

    // Create an array of our social links for ease of setup
    $social_networks = array( 'twitter', 'facebook', 'youtube', 'googleplus', 'pinterest', 'linkedin' );

    // Loop through our networks to setup our fields
    foreach( $social_networks as $network ) {

	    $wp_customize->add_setting(
	        '_aia_' . $network . '_link',
	        array(
	            'default' => '',
	            'sanitize_callback' => '_aia_sanitize_customizer_url'
	        )
	    );
	    $wp_customize->add_control(
	        '_aia_' . $network . '_link',
	        array(
	            'label'   => sprintf( __( '%s Link', 'aia' ), ucwords( $network ) ),
	            'section' => '_aia_social_links_section',
	            'type'    => 'text',
	        )
	    );
    }

    // Add our Footer Customization section section
    $wp_customize->add_section(
        '_aia_footer_section',
        array(
            'title'    => __( 'Footer Customization', 'aia' ),
            'priority' => 90,
        )
    );

    // Add our copyright text field
    $wp_customize->add_setting(
        '_aia_copyright_text',
        array(
            'default'           => ''
        )
    );
    $wp_customize->add_control(
        '_aia_copyright_text',
        array(
            'label'       => __( 'Copyright Text', 'aia' ),
            'description' => __( 'The copyright text will be displayed beneath the menu in the footer.', 'aia' ),
            'section'     => '_aia_footer_section',
            'type'        => 'text',
            'sanitize'    => 'html'
        )
    );

   // Add first part of our footer CTA
    $wp_customize->add_setting(
        '_aia_cta_text_1',
        array(
            'default'           => ''
        )
    );
    $wp_customize->add_control(
        '_aia_cta_text_1',
        array(
            'label'       => __( 'Footer CTA Line 1', 'aia' ),
            'description' => __( 'This first line of the CTA text will be displayed above the phone number in the footer.', 'aia' ),
            'section'     => '_aia_footer_section',
            'type'        => 'text',
            'sanitize'    => 'html'
        )
    );   

  // Add first part of our footer CTA
    $wp_customize->add_setting(
        '_aia_cta_text_2',
        array(
            'default'           => ''
        )
    );
    $wp_customize->add_control(
        '_aia_cta_text_2',
        array(
            'label'       => __( 'Footer CTA Line 2', 'aia' ),
            'description' => __( 'This second line of the CTA text will be displayed above the phone number in the footer.', 'aia' ),
            'section'     => '_aia_footer_section',
            'type'        => 'text',
            'sanitize'    => 'html'
        )
    );   

       // Add our footer CTA Phone Message
    $wp_customize->add_setting(
        '_aia_cta_phone_text',
        array(
            'default'           => ''
        )
    );
    $wp_customize->add_control(
        '_aia_cta_phone_text',
        array(
            'label'       => __( 'Footer CTA Phone Message', 'aia' ),
            'description' => __( 'The text will be displayed beneath the phone number in the footer. (Toll-Free, etc.)', 'aia' ),
            'section'     => '_aia_footer_section',
            'type'        => 'text',
            'sanitize'    => 'html'
        )
    );   
}
add_action( 'customize_register', '_aia_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function _aia_customize_preview_js() {
	wp_enqueue_script( '_aia_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20130508', true );
}
add_action( 'customize_preview_init', '_aia_customize_preview_js' );

/**
 * Sanitize our customizer text inputs
 */
function _aia_sanitize_customizer_text( $input ) {
    return sanitize_text_field( force_balance_tags( $input ) );
}

/**
 * Sanitize our customizer URL inputs
 */
function _aia_sanitize_customizer_url( $input ) {
    return esc_url( $input );
}