<?php
	/*
	 * PART-CANDYBAR.PHP
	 * THIS TEMPLATE FILE IS MAINLY CALLED FROM A SHORTCODE
	 * ALL 'CANDYBAR' DESIGN ELEMENTS ARE DESIGNED TO BE FULL PAGE WIDTH
	 * THUS THIS FILE NEEDS TO CLOSE ANY CONTENT DIVS THAT CONTAIN IT
	 * AND REOPEN THOSE SAME DIVS AFTER IT
	 * IN ORDER TO ACHIEVE THE FIXED WITH CONTENT BEFORE AND AFTER IT.
	 */

	/**
	 * Example of HTML for a regular page.php/post.php
	 * Before this candybar template file
	 * <main>
	 * 	<div class="content">
	 *		<?php the_content(); ?>
	 * 	</div>
	 *	<aside>
	 *		//sidebar stuff
	 * 	</aside>
	 * </main>
	 */

	/**
	 * Example of HTML for a regular page.php/post.php
	 * Using this candybar template file
	 * <main>
	 * 	<div class="content">
	 *		<?php the_content(); ?>//the content before the candybar shortcode
	 * 	</div>
	 * </main>
	 * <div class="candybar">
	 *	//candybar content
	 * </div>
	 * <div>
	 *	<div class="content">
	 *		//the_content() that comes after the shortcode
	 *	</div>
	 *	<aside>
	 *		//sidebar stuff
	 * 	</aside>
	 * </div>
	 */
?>
<div id="candy-bar">
	<?php echo of_get_option('cta_bottom'); ?>
	<svg class="phone-icon"><use xlink:href="#icon-phone"></use></svg>
	<?php echo do_shortcode('[phone]'); ?>
</div>