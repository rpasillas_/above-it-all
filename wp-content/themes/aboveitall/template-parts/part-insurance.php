<div id="insurance">
	<div class="wrap">
		<div class="insurance">
			<span class="h3">Most Insurance Accepted</span>
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/united-healthcare.png" alt="United Healthcare">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/blue-cross-blue-shield.png" alt="Blue Cross Blue Shield">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/aetna.png" alt="AETNA">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/cigna.png" alt="Cigna">
		</div>
		<div class="accreditation">
			<span class="h3">Standards &amp; Best Practices</span>
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/carf.png" alt="Carf">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/cdhcs.png" alt="California Department of Health Care Services">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/naatp.png" alt="NAATP">
		</div>
	</div>
</div>