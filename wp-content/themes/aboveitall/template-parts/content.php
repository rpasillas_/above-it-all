<?php
/**
 * @package Above it All
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( sprintf( '<h1 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h1>' ); ?>
	</header><!-- .entry-header -->

	<div class="entry-image">
		<?php the_post_thumbnail(); ?>
	</div>

	<footer class="entry-footer">
		<?php _aia_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
