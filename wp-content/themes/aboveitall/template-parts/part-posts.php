<?php

	$args = array(
			'post_type'		=> 'post',
			'cat'			=> '-5',
			'posts_per_page'	=> 3,
			'orderby' 		=> 'date',
			'order'			=> 'DESC',
			'ignore_sticky_posts'	=> true
		);
	$posts_query = new WP_Query( $args );		
?>

<div id="posts">
	<div class="wrap">
		<span class="h1">Alumni Success Stories</span>
		<svg><use xlink:href="#icon-aia-logo-mountain"></use></svg>
		<p>Watch videos from real Above It All clients sharing their experiences or read testimonals from clients and their families.</p>

		<?php if ( $posts_query -> have_posts() ) : ?>

			<div class="blogs-wrap">
			<?php while ( $posts_query -> have_posts() ) : $posts_query ->  the_post(); ?>

				<article class="post">
					<div class="content">
						<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
						<?php _aia_posted_on(); ?>
					</div>
					<div class="thumbnail">
						<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'featured' ); ?></a>
					</div>
				</article>

			<?php endwhile; ?>

			</div>
		<?php endif; ?>
		<?php wp_reset_postdata(); ?>
		
		<a href="<?php  echo get_permalink( get_option('page_for_posts') ); ?>" class="btn">Read More Testimonials</a>

	</div>
</div>