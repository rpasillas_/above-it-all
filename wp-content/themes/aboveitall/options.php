<?php
/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 */
function optionsframework_option_name() {
	// Change this to use your theme slug
	return 'options-framework-theme';
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 *
 * If you are making your theme translatable, you should replace 'theme-textdomain'
 * with the actual text domain for your theme.  Read more:
 * http://codex.wordpress.org/Function_Reference/load_theme_textdomain
 */

function optionsframework_options() {

	// Pull all the pages into an array
	$options_pages = array();
	$options_pages_obj = get_pages( 'sort_column=post_parent,menu_order' );
	$options_pages[''] = 'Select a page:';
	foreach ($options_pages_obj as $page) {
		$options_pages[$page->ID] = $page->post_title;
	}




	$options = array();


	/* =========================*\
	 *	CONTACT INFO
	\* =========================*/
	$options[] = array(
		'name' => 'Contact Info',
		'type' => 'heading'
	);

	$options[] = array(
		'name' => 'Phone',
		'desc' => '',
		'id' => 'phone',
		'class' => 'mini',
		'type' => 'text'
	);

	$options[] = array(
		'name' => 'Phone: Non-Admissions',
		'desc' => '',
		'id' => 'phone_alt',
		'class' => 'mini',
		'type' => 'text'
	);

	$options[] = array(
		'name' => 'Fax',
		'desc' => '',
		'id' => 'fax',
		'class' => 'mini',
		'type' => 'text'
	);

	$options[] = array(
		'name' => 'Email',
		'desc' => '',
		'id' => 'email',
		'class' => 'mini',
		'type' => 'text'
	);


	/* =========================*\
	 *	Address 1
	\* =========================*/
	$options[] = array(
		'name' => 'Address',
		'type' => 'heading'
	);

	$options[] = array(
		'name' => 'Street Address',
		'desc' => '',
		'id' => 'address',
		'type' => 'text'
	);

	$options[] = array(
		'name' => 'City',
		'desc' => '',
		'id' => 'city',
		'type' => 'text'
	);

	$options[] = array(
		'name' => 'State',
		'desc' => '',
		'id' => 'state',
		'class' => 'mini',
		'type' => 'text'
	);

	$options[] = array(
		'name' => 'Zip Code',
		'desc' => '',
		'id' => 'zip',
		'class' => 'mini',
		'type' => 'text'
	);


	/* =========================*\
	 *	Address 2
	\* =========================*/
	$options[] = array(
		'name' => 'Address (UPS & FedEx)',
		'type' => 'heading'
	);

	$options[] = array(
		'name' => 'Street Address',
		'desc' => '',
		'id' => 'address_1',
		'type' => 'text'
	);

	$options[] = array(
		'name' => 'City',
		'desc' => '',
		'id' => 'city_1',
		'type' => 'text'
	);

	$options[] = array(
		'name' => 'State',
		'desc' => '',
		'id' => 'state_1',
		'class' => 'mini',
		'type' => 'text'
	);

	$options[] = array(
		'name' => 'Zip Code',
		'desc' => '',
		'id' => 'zip_1',
		'class' => 'mini',
		'type' => 'text'
	);


	/* =========================*\
	 *	Address 3
	\* =========================*/
	$options[] = array(
		'name' => 'Address (USPS)',
		'type' => 'heading'
	);

	$options[] = array(
		'name' => 'Street Address',
		'desc' => '',
		'id' => 'address_2',
		'type' => 'text'
	);

	$options[] = array(
		'name' => 'City',
		'desc' => '',
		'id' => 'city_2',
		'type' => 'text'
	);

	$options[] = array(
		'name' => 'State',
		'desc' => '',
		'id' => 'state_2',
		'class' => 'mini',
		'type' => 'text'
	);

	$options[] = array(
		'name' => 'Zip Code',
		'desc' => '',
		'id' => 'zip_2',
		'class' => 'mini',
		'type' => 'text'
	);

	
	/* =========================*\
	 *	PPC
	\* =========================*/

	$options[] = array(
		'name' => 'PPC Settings',
		'type' => 'heading'
	);

			$options[] = array(
				'name' => 'Google Analytics',
				'desc' => '',
				'id' => 'ga_analytics',
				'type' => 'textarea'
			);

			$options[] = array(
				'name' => 'Google Remarketing',
				'desc' => '',
				'id' => 'g_remarketing',
				'type' => 'textarea'
			);

			$options[] = array(
				'name' => 'Call Tracking',
				'desc' => '',
				'id' => 'call_tracking',
				'type' => 'textarea'
			);

			$options[] = array(
				'name' => 'Call Tracking Span Class',
				'desc' => '',
				'id' => 'call_tracking_span_class',
				'type' => 'text',
				'class' => 'mini'
			);

			$options[] = array(
				'name' => 'SnapEngage',
				'desc' => '',
				'id' => 'snap_engage',
				'type' => 'textarea'
			);



	/* =========================*\
	 *	CONTENT
	\* =========================*/
	$options[] = array(
		'name' => 'Content Settings',
		'type' => 'heading'
	);
		$options[] = array(
			'name' => 'CTA Bottom',
			'id' => 'cta_bottom',
			'type' => 'textarea'
		);
		$options[] = array(
			'name' => 'Blog Page Introduction',
			'id' => 'blog_intro',
			'type' => 'textarea'
		);
		



	/* =========================*\
	 *	SECTION BANNERS
	\* =========================*/

	$options[] = array(
		'name' => 'Section Banners',
		'type' => 'heading'
	);

		//---- TREATMENT PROGRAMS
		$options[] = array(
			'name'	=> 'Banner Title: Treatment Programs',
			'desc'	=> '',
			'id' 	=> 'banner_title_51',
			'std'	=> 'Treatment Programs',
			'type' 	=> 'text'
		);
		$options[] = array(
			'name' 	=> 'Banner Image: Treatment Programs',
			'desc' 	=> '',
			'id' 	=> 'banner_image_51',
			'type' 	=> 'upload'
		);


		//---- REHABILITATION PROCESS
		$options[] = array(
			'name'	=> 'Banner Title: Rehabilitation Process',
			'desc'	=> '',
			'id' 	=> 'banner_title_52',
			'std'	=> 'Rehabilitation Process',
			'type' 	=> 'text'
		);
		$options[] = array(
			'name' 	=> 'Banner Image: Rehabilitation Process',
			'desc' 	=> '',
			'id' 	=> 'banner_image_52',
			'type' 	=> 'upload'
		);


		//---- EXPERIENCE ABOVE IT ALL
		$options[] = array(
			'name'	=> 'Banner Title: Experience Above It All',
			'desc'	=> '',
			'id' 	=> 'banner_title_85',
			'std'	=> 'Experience Above It All',
			'type' 	=> 'text'
		);
		$options[] = array(
			'name' 	=> 'Banner Image: Experience Above It All',
			'desc' 	=> '',
			'id' 	=> 'banner_image_85',
			'type' 	=> 'upload'
		);


		//---- COMMUNITY
		$options[] = array(
			'name'	=> 'Banner Title: Community',
			'desc'	=> '',
			'id' 	=> 'banner_title_4456',
			'std'	=> 'Community',
			'type' 	=> 'text'
		);
		$options[] = array(
			'name' 	=> 'Banner Image: Community',
			'desc' 	=> '',
			'id' 	=> 'banner_image_4456',
			'type' 	=> 'upload'
		);


		//---- BLOG
		$options[] = array(
			'name' 	=> 'Banner Title: Blog',
			'desc' 	=> '',
			'id' 	=> 'banner_title_blog',
			'std'	=> 'Blog',
			'type' 	=> 'text'
		);
		$options[] = array(
			'name' 	=> 'Banner Image: Blog',
			'desc' 	=> '',
			'id' 	=> 'banner_image_blog',
			'type' 	=> 'upload'
		);


		//---- CONTACT
		//---- ASSUMES A CONTACT PAGE WITH PAGE ID 3
		$options[] = array(
			'name' 	=> 'Banner Title: Contact',
			'desc' 	=> '',
			'id' 	=> 'banner_title_41',
			'std'	=> 'Contact',
			'type' 	=> 'text'
		);
		$options[] = array(
			'name' 	=> 'Banner Image: Contact',
			'desc' 	=> '',
			'id' 	=> 'banner_image_41',
			'type' 	=> 'upload'
		);








	return $options;
}
