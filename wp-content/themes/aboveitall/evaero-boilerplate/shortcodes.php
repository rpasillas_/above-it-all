<?php
/**
 * Evaero Shortcodes
 * Is dependent on theme options
 *
 * @package WordPress
 * @subpackage evaero-boilerplate
 * @since Evaero Boilerplate 1.0
 *
 */


//----- Panel
		function sc_panel($atts, $content = null) {
			extract( shortcode_atts(
				array(
					'title' => 'panel',
				),
				$atts ) );
			return '<div class="panel panel-default">
						<div class="panel-heading">' . esc_attr($title) . '</div>
						<div class="panel-body">
							<p>' . $content . '</p>
						</div>
					</div>';
		}
		add_shortcode('panel', 'sc_panel');

//----- Well
		function sc_well($atts, $content = null) {
			return '<div class="well">' . $content . '</div>';
		}
		add_shortcode('well', 'sc_well');

//----- Blockquote with Author
		function sc_blockquote($atts, $content = null) {
			extract(  shortcode_atts(
				array(
					'author' => 'blockquote',
				), $atts ) );

			return '<blockquote><p>' . $content . '</p><span class="author">' . esc_attr($author) . '</span></blockquote>';
		}
		add_shortcode('blockquote', 'sc_blockquote');

//----- Pullquote: Right
		function sc_pullquote($atts, $content = null) {
			return '<span class="pullquote right">' . $content . '</span>';
		}
		add_shortcode('pullquote', 'sc_pullquote');
		add_shortcode('pullquote_right', 'sc_pullquote');

//----- Pullquote: Left
		function sc_pullquote_left($atts, $content = null) {
			return '<span class="pullquote left">' . $content . '</span>';
		}
		add_shortcode('pullquote_left', 'sc_pullquote_left');

//----- Double-Column List
		function sc_twocol($atts, $content = null) {
			return '<div class="double clear">' . $content . '</div>';
		}
		add_shortcode('twocol', 'sc_twocol');

//----- Button
		function sc_button($atts, $content = null) {
			extract( shortcode_atts(
				array(
					'id' => 'button',
				),
				$atts ) );
			return '<a href="<?php echo get_page_link( \'' . esc_attr($id) . '\' ); ?>">' . $content . '</a>';
		}
		add_shortcode('button', 'sc_button');

//----- CTA: Giant
		function sc_cta_giant($atts, $content = null) {
			return '<span class="cta giant">' . $content . '<br>' . do_shortcode('[phone]') . '</span>';
		}
		add_shortcode('cta_giant', 'sc_cta_giant');

//----- CTA: Large
		function sc_cta_large($atts, $content = null) {
			return '<span class="cta large">' . $content . '<br>' . do_shortcode('[phone]') . '</span>';
		}
		add_shortcode('cta_large', 'sc_cta_large');

//----- CTA: Regular
		function sc_cta($atts, $content = null) {
			return '<span class="cta">' . $content  . '<br>' . do_shortcode('[phone]') . '</span>';
		}
		add_shortcode('cta', 'sc_cta');

//----- CTA: Small
		function sc_cta_small($atts, $content = null) {
			return '<span class="cta small">' . $content . '<br>' . do_shortcode('[phone]') . '</span>';
		}
		add_shortcode('cta_small', 'sc_cta_small');

//----- CTA: Mini
		function sc_cta_mini($atts, $content = null) {
			return '<span class="cta mini">' . $content . '<br>' . do_shortcode('[phone]') . '</span>';
		}
		add_shortcode('cta_mini', 'sc_cta_mini');

//----- CTA: Call Now
		function sc_cta_call_now($atts, $content = null) {
			return '<div class="call-now">' . $content . of_get_option( 'cta_bottom' ). '</div>';
		}
		add_shortcode('call-now', 'sc_cta_call_now');

//----- CTA: Bottom
		function sc_cta_bottom($atts, $content = null) {
			return '<div class="cta-bottom">' . of_get_option( 'cta_bottom' ). ' ' . do_shortcode('[phone]') . '</div>';
		}
		add_shortcode('bottom', 'sc_cta_bottom');
		add_shortcode('cta_bottom', 'sc_cta_bottom');

//----- Site URL
		function sc_site_url($atts, $content = null) {
			return 'http://';
		}
		add_shortcode('site-url', 'sc_site_url');

//----- ClearFIX
		function sc_clear($atts, $content = null) {
			return '<span class="clear">' . $content . '</span>';
		}
		add_shortcode('clear', 'sc_clear');

//----- Candybar
/**
 * Inserts a "candybar" template file into the content.
 * Any design element in the PSD comps that interrupt the page content
 * to display a call to action will need to put into its own template file
 * so that this shortcode can insert it anywhere in the content.
 * often times these "candybars" span the entire width of the browser
 * in these instances, the template file will need to begin with enough closing div tags
 * and end with enough opening div tags to allow the "candybar" to close the main content divs and allow
 * it span the entire browser width.
 */
		function candybar($atts){
			$data = '';
			ob_start();
			get_template_part('template-parts/part-candybar');
			$data = ob_get_contents();
			ob_end_clean();
			return $data;
		}
		add_shortcode('cta_banner', 'candybar');

//----- Address
		function sc_address($atts, $content = null) {
			$data = of_get_option( 'address' ) . '<br>';
			$data .= of_get_option( 'city' ) . ', ';
			$data .= of_get_option( 'state' ) . ' ';
			$data .= of_get_option( 'zip' );
			return $data;
		}
		add_shortcode('address', 'sc_address');

//----- Email
		function sc_email($atts, $content = null) {
			return '<a href="mailto:' . of_get_option( 'email' ) . '">' . of_get_option( 'email' ) . '</a>';
		}
		add_shortcode('email', 'sc_email');

//----- Fax
		function sc_fax($atts, $content = null) {
			return $site_fax;
		}
		add_shortcode('fax', 'sc_fax');

//----- Phone
		function sc_phone($atts, $content = null) {
			$span  = of_get_option( 'call_tracking_span_class' );
			$num   = of_get_option( 'phone' );
			$rid   = array('-','(',')',' ');
			$clean = (str_replace($rid,'',$num));
			if ($span != '') {
				return '<span class="desktop phone ' . $span . ' clickable"><span class="num">' . $num . '</span></span>';
			} else if (strpos($num,'+') !== 'false') {
				return '<a class="desktop phone" href="tel:' . $clean . '"><span class="num">' . $num . '</span></a>';
			} else {
				return '<a class="desktop phone" href="tel:+' . $clean . '"><span class="num">' . $num . '</span></a>';
			}
		}
		add_shortcode('phone', 'sc_phone');

//----- Phone [mobile: shows phone icon]
		function sc_phone_mobile($atts, $content = null) {
			$span  = of_get_option( 'call_tracking_span_class' );
			$num   = of_get_option( 'phone' );
			$rid   = array('-','(',')',' ');
			$clean = (str_replace($rid,'',$num));
			if ($span != '') {
				return '<span class="mobile ' . $span . ' clickable"><span class="num"></span></span>';
			} else if (strpos($num,'+') !== 'false') {
				return '<a class="mobile" href="tel:' . $clean . '"><span class="num"></span></a>';
			} else {
				return '<a class="mobile" href="tel:+' . $clean . '"><span class="num"></span></a>';
			}
		}
		add_shortcode('phone_mobile', 'sc_phone_mobile');
