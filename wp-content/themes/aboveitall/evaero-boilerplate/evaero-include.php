<?php

      /**
       * LINEAGE FUNCTIONS
       * Displays a page's parent
       * Title and Banner Image.
       */
      require_once dirname(__FILE__) . '/lineage.php';


      /**
       * SIBLINGS WIDGET
       * Displays a page's siblings as
       * a navigation
       */
      require_once dirname(__FILE__) . '/siblings.php';


      /**
       * SHORTCODES
       * Common shortcodes used by Evaero.
       *
       */
      require_once dirname(__FILE__) . '/shortcodes.php';


      /**
       * NAV WALKER
       * Used by the slider custom post type
       * to build a select list of Pages & Posts
       */
      require_once dirname(__FILE__) . '/select_walker_page.php';


      /**
       * HOME PAGE SLIDER
       * Custom Post Type for home page slider.
       *
       */
      //require_once dirname(__FILE__) . '/slider_custom_post_type.php';


      /**
       * STAFF
       * Custom Post Type staff.
       *
       */
      require_once dirname(__FILE__) . '/staff_custom_post_type.php';


      /**
       * HEADER SCRIPTS
       * Echoes scripts from theme options into the header
       *
       */
      require_once dirname(__FILE__) . '/header_scripts.php';      


       /**
       * BODY SCRIPTS
       * Echoes scripts into the opening of the body tag via a custom hook
       *
       */
      require_once dirname(__FILE__) . '/body_scripts.php';      