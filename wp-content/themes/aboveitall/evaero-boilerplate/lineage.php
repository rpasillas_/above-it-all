<?php
/**
 * Returns the ID of the top root ancestor of a page.
 *
 * @package WordPress
 * @subpackage evaero-boilerplate
 * @since Evaero Boilerplate 1.0
 *
 * @return integer|empty string of the post ancestor's ID or the post ID, or empty string as default.
 */
function get_top_parent_page_id() {
	global $post;
	$ancestors = $post->ancestors;
	if ($ancestors) {
		return end($ancestors);
	} else if( $post ) {
		return $post->ID;
	}else{
		return '';
	}
}

/**
 * Returns 'section' title.
 *
 * @package WordPress
 * @subpackage evaero-boilerplate
 * @since Evaero Boilerplate 1.0
 *
 * @return string value of the appropriate theme option of either the blog section, 
 *	     or a specific page section and defaults to the page title if no conditions met.
 */
function e_lineage_root_parent_title() {
	$id = get_top_parent_page_id();

	if (is_home() || is_single() || is_archive() || is_category()) {
		return of_get_option( 'banner_title_blog' );
	} else if (is_404()) {
		return 'Page Not Found';
	} else if ($id) {
		return of_get_option( 'banner_title_' . $id );
	} else {
		wp_title();
	}
}

/**
 * Returns 'section' image.
 *
 * @package WordPress
 * @subpackage evaero-boilerplate
 * @since Evaero Boilerplate 1.0
 *
 * @return string value of the appropriate theme option of either the blog section, 
 *	     or a specific page section.
 */
function e_lineage_root_parent_img() {
	$id = get_top_parent_page_id();

	$image = '';

	if (is_home() || is_single() || is_archive()) {
		$image = of_get_option( 'banner_image_blog' );

		if($image){
			$image = ' style="background-image:url(\'' . $image . '\');"';
		}
	} else if ($id) {
		$image = of_get_option( 'banner_image_' . $id );

		if( $image ){
			$image = ' style="background-image:url(\'' . $image . '\');"';
		}

	}
	return $image;
}
