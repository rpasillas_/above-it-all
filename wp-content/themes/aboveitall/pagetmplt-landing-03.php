<?php
	/* Template Name: Landing Page #3 */
?>

<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#" lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=688">
		<link rel="shortcut icon" href="favicon.ico">

		<title><?php wp_title(); ?></title>

		<meta prefix="og: http://ogp.me/ns#" property="og:title" content="<?php echo get_the_title(); ?>"/>
		<meta prefix="og: http://ogp.me/ns#" property="og:image" content="<?php thumb_url(); ?>"/>
		<link rel="image_src" href="<?php thumb_url(); ?>" />

		<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/style-landing.css" rel="stylesheet">
		<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/style-landing-03.css" rel="stylesheet">

		<?php if ( ot_get_option( 'ppc_google_analytics' ) ) echo '<script type="text/javascript">' . "\n" . ot_get_option( 'ppc_google_analytics' ) . "\n\t\t" . '</script>'; ?>
		<?php if ( ot_get_option( 'ppc_google_remarketing' ) ) echo ot_get_option( 'ppc_google_remarketing' ); ?>
		<?php if ( ot_get_option( 'ppc_infinity' ) ) echo ot_get_option( 'ppc_infinity' ); ?>

		<script async src="//5239.tctm.co/t.js"></script>

		<?php wp_head(); ?>
	</head>

	<body <?php body_class(); ?>>

		<nav>
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<a href="/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/landing/logo-01.png" /></a>
					</div>
					<div class="col-md-9">
						<span class="call"><span class="first">Have Questions?</span> Call Us at <?php echo do_shortcode('[phone]'); ?></span>
					</div>
				</div>
			</div>
		</nav>

		<header>
			<div class="container">
				<div class="callout">
					<div class="pre-form toggle">
						<span class="h1">Naturally Effective <br/>Recovery</span>
						<span class="go-form desktop-only"><div class="border"><span>Contact Us Now</span> for total recovery from Drug &amp; Alcohol Addiction</div></span>
						<span class="go-form mobile-only"><div class="border"><span>Contact Us Now</span> for total recovery from Drug &amp; Alcohol Addiction</div></span>
					</div>
					<div id="form" class="form toggle">
						<p class="instructions">Enter your information to have an advisor contact you.</p>
						<?php echo do_shortcode('[contact-form-7 id="3608" title="Landing Page #1"]'); ?>
					</div>
					<div id="thanks" class="thanks">
						<p>Thank you for contacting us! One of our advisors will be in touch with you shortly.</p>
					</div>
				</div>
			</div>
		</header>

		<!-- <header>
			<div class="container">
				<div class="intro">
					<div class="left">
						<span class="h1">Naturally Effective Recovery</span>
					</div>
					<div class="spacer"></div>
					<div class="right go-form toggle">
						<span class="desktop-only"><span>Contact Us Now</span> for total recovery from <br/>Drug &amp; Alcohol Addiction</span>
						<span class="mobile-only"><span>Contact Us Now</span> for total recovery from <br/>Drug &amp; Alcohol Addiction</span>
					</div>
					<div id="form" class="form toggle">
						<p class="instructions">Enter your information to have an advisor contact you.</p>
						<?php echo do_shortcode('[contact-form-7 id="3608" title="Landing Page #1"]'); ?>
					</div>
					<div id="thanks" class="thanks">
						<p>Thank you for contacting us! One of our advisors will be in touch with you shortly.</p>
					</div>
				</div>
			</div>
		</header> -->

		<footer>
			<div class="mountains">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<span class="title"><?php echo ot_get_option( 'footer_mountains_title' ); ?></span>
							<p class="content-top"><?php echo ot_get_option( 'footer_mountains_content_top' ); ?></p>
						</div>
					</div>
					<div class="col-md-12 video">
						<iframe width="750" height="455" src="//www.youtube.com/embed/_mieen64dSA?autoplay=0&amp;showinfo=0" frameborder="0" allowfullscreen=""></iframe>
						<img class="map" src="<?php echo get_template_directory_uri(); ?>/img/landing/map.jpg" />
					</div>
					<div class="row">
						<div class="col-md-6">
							<p class="content-bottom">For more information on how Above It All Treatment can help you take that first step to recovery from an addiction, please contact us today. <strong>We're here 24/7 to help.</strong></p>
							<a class="get-help desktop-only" href="http://aboveitalltreatment.com/contact/"><div class="inner">Change Your Life <span>Call Us Today</span></div></a>
							<a class="get-help mobile-only" href="tel:877-941-0879"><div class="inner">Change Your Life <span>Call Us Today</span></div></a>
						</div>
						<div class="col-md-6">
						</div>
					</div>
				</div>
			</div>
			<div class="bottom">
				<div class="container">
					<div class="col-md-6 social">
						<p><span>Get Social With AIA</span></p>
						<ul>
							<li><a href="https://www.facebook.com/aboveitalltreatmentcenter " target="_blank" class="facebook">Facebook</a></li>
							<li><a href="http://www.twitter.com/aboveitall_TC " target="_blank" class="twitter">Twitter</a></li>
							<li><a href="https://plus.google.com/+Aboveitalltreatment" target="_blank" class="gplus">Google+</a></li>
							<li><a href="https://www.pinterest.com/aboveitallca" target="_blank" class="pinterest">Pinterest</a></li>
							<li><a href="http://www.youtube.com/user/aboveitallCA" target="_blank" class="youtube">YouTube</a></li>
							<li><a href="https://linkedin.com/company/above-it-all-drug-&amp;-alcohol-treatment-center" target="_blank" class="linkedin">LinkedIn</a></li>
						</ul>
					</div>
					<div class="col-md-6 hotline">
						<span class="call">
							<span class="one">Help is waiting.</span>
							<span class="two">Call us today!</span>
							<a href="tel:8779410879">877-941-0879</a>
							<span class="three">24/7 Confidential Hotline</span>
						</span>
					</div>
				</div>
			</div>
		</footer>

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/bootstrap.min.js"></script>
		<script>
			$( ".go-form" ).click(function() {
				$( ".toggle" ).toggle();
			});
		</script>

		<?php wp_footer(); ?>
	</body>
</html>