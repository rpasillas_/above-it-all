<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Above it All
 */
?>

<div class="secondary widget-area" role="complementary">
	<?php if (  is_active_sidebar( 'sidebar-pages' ) && is_page()  ) : ?>

		<?php dynamic_sidebar( 'sidebar-pages' ); ?>

	<?php elseif( is_active_sidebar( 'sidebar-posts' ) && !is_page() ) : ?>

		<?php dynamic_sidebar( 'sidebar-posts' ); ?>

		<div class="fb-page" data-href="https://www.facebook.com/aboveitalltreatmentcenter" data-tabs="timeline" data-width="358" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/aboveitalltreatmentcenter"><a href="https://www.facebook.com/aboveitalltreatmentcenter">Above It All Treatment Center</a></blockquote></div></div>

	<?php endif; ?>
</div><!-- .secondary -->
